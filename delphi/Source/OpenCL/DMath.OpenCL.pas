unit DMath.OpenCL;

interface

uses
  System.SysUtils;

type
  EOpenCLError = class(Exception);

function OpenCL_Loaded: Boolean;

procedure Run(const FileName: string; const Input1, Input2, Output: TArray<Single>;
  ARows, ACols, BCols: Integer);

implementation

uses
  CL,
  DelphiCL;

var
  SkipOpenCL: Boolean;
  Platforms: TDCLPlatforms;
  Device: PDCLDevice;
  Queue: TDCLCommandQueue;
  Prog: TDCLProgram;
  Kernel: TDCLKernel;

function OpenCL_Loaded: Boolean;
begin
  if Assigned(Platforms) then
    Exit(True);
  if SkipOpenCL then
    Exit(False);

//  InitOpenCL('c:\Windows\SysWOW64\OpenCL.dll');
  InitOpenCL;
  Platforms := TDCLPlatforms.Create;
  Result := True;
end;

procedure Run(const FileName: string; const Input1, Input2, Output: TArray<Single>;
  ARows, ACols, BCols: Integer);
var
  i: Integer;
  InputBuffer1, InputBuffer2, OutputBuffer: TDCLBuffer;
begin
  if not Assigned(Platforms) then
    raise EOpenCLError.Create('OpenCL not initialized');

  try
    if Device = nil then
    begin
      for i := 0 to Platforms.Platforms[3].DeviceCount - 1 do
        Writeln(Platforms.Platforms[3].Devices[i].Name);
      Device := Platforms.Platforms[0].DeviceWithMaxComputeUnits;
    end;
    if Queue = nil then
      Queue := Device.CreateCommandQueue;
    InputBuffer1 := Device.CreateBuffer(SizeOf(Input1[0]) * Length(Input1), @Input1[0],
      [mfReadOnly, mfUseHostPtr]);
    InputBuffer2 := Device.CreateBuffer(SizeOf(Input2[0]) * Length(Input2), @Input2[0],
      [mfReadOnly, mfUseHostPtr]);
    OutputBuffer := Device.CreateBuffer(SizeOf(Output[0]) * Length(Output), @Output[0],
      [mfWriteOnly]);
    // OutputBuffer := Device.CreateBuffer(SizeOf(Output[0]) * Length(Output), @Output[0], [mfReadWrite, mfUseHostPtr]);

    if Prog = nil then
    begin
      Prog := Device.CreateProgram(ExtractFilePath(ParamStr(0)) + FileName);
      Prog.SaveToFile(AnsiString(FileName + '.bin'));
    end;
    if Kernel = nil then
      Kernel := Prog.CreateKernel('dmath');
    Kernel.SetArg(0, InputBuffer1);
    Kernel.SetArg(1, InputBuffer2);
    Kernel.SetArg(2, SizeOf(ACols), @ACols);
    Kernel.SetArg(3, SizeOf(BCols), @BCols);
    Kernel.SetArg(4, OutputBuffer);
    Queue.Execute(Kernel, [ARows, BCols]);
    Queue.ReadBuffer(OutputBuffer, SizeOf(Output[0]) * Length(Output), @Output[0]);
  finally
    InputBuffer1.Free;
    InputBuffer2.Free;
    OutputBuffer.Free;
  end;
end;

initialization

finalization

Queue.Free;
Platforms.Free;
Prog.Free;
Kernel.Free;

end.
