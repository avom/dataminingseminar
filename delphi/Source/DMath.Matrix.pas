unit DMath.Matrix;

interface

{$EXCESSPRECISION OFF}

uses
  System.SysUtils;

type
  EDMathException = class(Exception);

  TMatrixElementType = Single;
  PMatrixElementType = ^TMatrixElementType;

  TMatrix = record
  private
    FElems: TArray<TMatrixElementType>;
    FRows: Integer;
    FCols: Integer;
    function GetElems(Row, Col: Integer): TMatrixElementType; inline;
    procedure SetElems(Row, Col: Integer; Value: TMatrixElementType); inline;
  public
    constructor Create(Rows, Cols: Integer);

    function Transpose: TMatrix;

    procedure SetRow(RowIndex: Integer; const RowElems: array of TMatrixElementType);
    function ToString: string;

    property Elems[Row, Col: Integer]: TMatrixElementType read GetElems write SetElems; default;
    property Cols: Integer read FCols;
    property Rows: Integer read FRows;

    class function CreateRandom(Rows, Cols: Integer): TMatrix; static;

    class operator Equal(const A, B: TMatrix): Boolean;
    class operator Multiply(const A, B: TMatrix): TMatrix;
  end;

implementation

uses
  DMath.Matrix.Multiplication;

{ TMatrix }

constructor TMatrix.Create(Rows, Cols: Integer);
begin
  SetLength(FElems, Rows * Cols);
  FRows := Rows;
  FCols := Cols;
end;

class function TMatrix.CreateRandom(Rows, Cols: Integer): TMatrix;
var
  Row, Col: Integer;
begin
  Result := TMatrix.Create(Rows, Cols);
  for Row := 0 to Rows - 1 do
  begin
    for Col := 0 to Cols - 1 do
      Result.FElems[Row * Cols + Col] := Random;
  end;
end;

class operator TMatrix.Equal(const A, B: TMatrix): Boolean;
var
  i: Integer;
begin
  Result := (A.Rows = B.Rows) and (A.Cols = B.Cols);
  if not Result then
    Exit;

  for i := 0 to A.Rows * A.Cols - 1 do
  begin
    if A.FElems[i] <> B.FElems[i] then
      Exit(False);
  end;
end;

function TMatrix.GetElems(Row, Col: Integer): TMatrixElementType;
begin
  Result := FElems[Row * Cols + Col];
end;

class operator TMatrix.Multiply(const A, B: TMatrix): TMatrix;
var
  Elems: TArray<TMatrixElementType>;
begin
  if A.Cols <> B.Rows then
    raise EDMathException.Create('Dimensions of matrices are not suitable for multiplication');

//  Result.FElems :=
//    DMath.Matrix.Multiplication.Multiply(A.FElems, B.FElems, A.Rows, A.Cols, B.Cols);
  Result.FElems :=
    DMath.Matrix.Multiplication.Multiply(A.FElems, B.Transpose.FElems, A.Rows, A.Cols, B.Cols);
  Result.FRows := A.Rows;
  Result.FCols := B.Cols;
end;

procedure TMatrix.SetElems(Row, Col: Integer; Value: TMatrixElementType);
begin
  FElems[Row * Cols + Col] := Value;
end;

procedure TMatrix.SetRow(RowIndex: Integer; const RowElems: array of TMatrixElementType);
var
  i: Integer;
begin
  if Length(RowElems) <> Cols then
    raise EDMathException.Create('Array and matrix row dimensions do not match');

  for i := 0 to Cols - 1 do
    FElems[RowIndex * Cols + i] := RowElems[i];
end;

function TMatrix.ToString: string;
var
  Col, Row: Integer;
begin
  Result := '';
  for Row := 0 to Rows - 1 do
  begin
    for Col := 0 to Cols - 1 do
      Result := Result + Format('%.6g ', [FElems[Row * Cols + Col]]);
    Result := Result + sLineBreak;
  end;
end;

function TMatrix.Transpose: TMatrix;
var
  Row, Col: Integer;
begin
  Result := TMatrix.Create(Cols, Rows);
  for Row := 0 to Rows - 1 do
  begin
    for Col := 0 to Cols - 1 do
      Result.FElems[Col * Rows + Row] := FElems[Row * Cols + Col];
  end;
end;

end.
