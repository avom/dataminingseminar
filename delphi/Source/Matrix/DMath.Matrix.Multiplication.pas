unit DMath.Matrix.Multiplication;

interface

uses
  DMath.Matrix;

function Multiply(const AElems, BElems: TArray<TMatrixElementType>;
  ARows, ACols, BCols: Integer): TArray<TMatrixElementType>;

implementation

uses
  DMath.OpenCL;

{ TMatrixMultiplication }

function MultiplyOnCpu(const AElems, BElems: TArray<TMatrixElementType>;
  ARows, ACols, BCols: Integer): TArray<TMatrixElementType>;
var
  Col, Row: Integer;
  i: Integer;
  Value: TMatrixElementType;
  BT: TMatrix;
  Ai, BTi: PMatrixElementType;
begin
  SetLength(Result, ARows * BCols);
  for Row := 0 to ARows - 1 do
  begin
    for Col := 0 to BCols - 1 do
    begin
      Value := 0.0;
      i := ACols;
      Ai := @AElems[Row * ACols];
      BTi := @BElems[Col * ACols];
      while i > 0 do
      begin
        Value := Value + Ai^ * BTi^;
        Dec(i);
        Inc(Ai);
        Inc(BTi);
      end;
      Result[Row * BCols + Col] := Value;
    end;
  end;
end;

function MultiplyOnGpu(const AElems, BElems: TArray<TMatrixElementType>;
  ARows, ACols, BCols: Integer): TArray<TMatrixElementType>;
const
  ArrayLength = 1024;
var
  A, B, C: TArray<Single>;
  i: Integer;
begin
  SetLength(Result, ARows * BCols);
  Run('matrix_mul.cl', AElems, BElems, Result, ARows, ACols, BCols);


//  SetLength(A, ArrayLength);
//  SetLength(B, ArrayLength);
//  SetLength(C, ArrayLength);
//  for i := 0 to ArrayLength - 1 do
//  begin
//    A[i] := i;
//    B[i] := ArrayLength + i;
//  end;
//  Run('matrix_mul.cl', A, B, C);
end;

function Multiply(const AElems, BElems: TArray<TMatrixElementType>;
  ARows, ACols, BCols: Integer): TArray<TMatrixElementType>;
begin
  if OpenCL_Loaded then
    Result := MultiplyOnGpu(AElems, BElems, ARows, ACols, BCols)
  else
    Result := MultiplyOnCpu(AElems, BElems, ARows, ACols, BCols);
end;

end.
