unit DMathTests.MatrixTests;

interface

uses
  TestFramework;

type
  TMatrixTests = class(TTestCase)
  published
    procedure Multiplication;
    procedure Transpose;
  end;

implementation

uses
  SysUtils,
  DMath.Matrix;

{ TMatrixTests }

procedure TMatrixTests.Multiplication;
var
  A, B, C, D: TMatrix;
  Col, Row: Integer;
begin
  A := TMatrix.Create(3, 2);
  A.SetRow(0, [1, 2]);
  A.SetRow(1, [3, 4]);
  A.SetRow(2, [5, 6]);

  B := TMatrix.Create(2, 4);
  B.SetRow(0, [7, 8, 9, 10]);
  B.SetRow(1, [11, 12, 13, 14]);

  C := A * B;

  D := TMatrix.Create(3, 4);
  D.SetRow(0, [29, 32, 35, 38]);
  D.SetRow(1, [65, 72, 79, 86]);
  D.SetRow(2, [101, 112, 123, 134]);

  CheckEquals(3, C.Rows, 'Invalid number of rows');
  CheckEquals(4, C.Cols, 'Invalid number of columns');

  for Row := 0 to C.Rows - 1 do
  begin
    for Col := 0 to C.Cols - 1 do
      CheckEquals(D[Row, Col], C[Row, Col], 0.001, Format('Mismatch at %d:%d', [Row, Col]));
  end;
end;

procedure TMatrixTests.Transpose;
var
  A, AT: TMatrix;
begin
  A := TMatrix.Create(2, 4);
  A.SetRow(0, [0, 1, 2, 3]);
  A.SetRow(1, [4, 5, 6, 7]);

  AT := A.Transpose;

  CheckEquals(4, AT.Rows, 'Invalid number of rows');
  CheckEquals(2, AT.Cols, 'Invalid number of columns');

  CheckEquals(0, AT[0, 0], 'Mismathc at 0:0');
  CheckEquals(4, AT[0, 1], 'Mismathc at 0:1');
  CheckEquals(1, AT[1, 0], 'Mismathc at 1:0');
  CheckEquals(5, AT[1, 1], 'Mismathc at 1:1');
  CheckEquals(2, AT[2, 0], 'Mismathc at 2:0');
  CheckEquals(6, AT[2, 1], 'Mismathc at 2:1');
  CheckEquals(3, AT[3, 0], 'Mismathc at 3:0');
  CheckEquals(7, AT[3, 1], 'Mismathc at 3:1');
end;

initialization

TestFramework.RegisterTest(TMatrixTests.Suite);

end.
