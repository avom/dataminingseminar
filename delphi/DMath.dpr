program DMath;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  Winapi.Windows,
  DMath.Matrix in 'Source\DMath.Matrix.pas',
  DMath.Matrix.Multiplication in 'Source\Matrix\DMath.Matrix.Multiplication.pas',
  DMath.OpenCL in 'Source\OpenCl\DMath.OpenCL.pas';

const
//  M = 100 * 1000;
//  N = 300;
  M = 10 * 1000;
  N = 300;
var
  A, B, C: TMatrix;
  Start: Cardinal;
begin
  RandSeed := 0;
  try
//    A := TMatrix.CreateRandom(M, N);
//    B := TMatrix.CreateRandom(N, N);
    A := TMatrix.Create(3, 2);
    A.SetRow(0, [1, 2]);
    A.SetRow(1, [3, 4]);
    A.SetRow(2, [5, 6]);
    B := TMatrix.Create(2, 4);
    B.SetRow(0, [1, 2, 3, 4]);
    B.SetRow(1, [5, 6, 7, 8]);
    C := A * B;
    Writeln(C.ToString);
//    Start := GetTickCount;
//    C := A * B;
//    Writeln((GetTickCount - Start) / 1000:0:2);
//    Start := GetTickCount;
//    C := A * B;
//    Writeln((GetTickCount - Start) / 1000:0:2);
//    Start := GetTickCount;
//    C := A * B;
//    Writeln((GetTickCount - Start) / 1000:0:2);
//    Writeln(C[0, 0]:0:3);
//    Writeln(C[100, 100]:0:3);
  except
    on Ex: Exception do
      Writeln(Ex.Message);
  end;
  Readln;
end.
