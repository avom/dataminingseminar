__kernel void dmath(__global float *a, __global float *b, int acols, int bcols, __global float *c)
{
	int tx = get_global_id(0);
	int ty = get_global_id(1);
	float value = 0;
	for (int k = 0; k < acols; k++)
	{
		float elementA = a[tx * acols + k];
		// float elementB = b[k * bcols + ty];
		float elementB = b[ty * acols + k];
		value += elementA * elementB;
	}
	c[tx * bcols + ty] = value;
}
