import codecs
import logging
import sys

import AnalogyTester
import settings
import WordEmbeddings

def add_file_logging(dir):
    log_file_name = dir + 'log.txt'

    logger_formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
    logger_filehandler = logging.FileHandler(log_file_name)
    logger_filehandler.setFormatter(logger_formatter)
    settings.logger.addHandler(logger_filehandler)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        settings.logger.info('USAGE: python est_ref_tests.py <procrustes|linear> <dimensions>')
        s_method = 'procrustes'
        s_dim = '50'
        settings.logger.info('Using method = ' + s_method + ' and dimensions = ' + s_dim)
    else:    
        s_method = sys.argv[1]
        s_dim = sys.argv[2]

    W_dir = 'h:\\DataMiningSeminar\\data\\alligaator\\w2v\\'
    Y_base_dir = 'h:\\DataMiningSeminar\\data\\alligaator\\'
    Y_dir = Y_base_dir + s_method + s_dim + '\\'
    W_filenames = ['w10.s' + s_dim + '.' + str(i) + '.bin' for i in range(10)]
    words_filename = W_dir + 'words.txt'

    add_file_logging(Y_dir)
    settings.logger.info("Running %s" % ' '.join(sys.argv))

    settings.logger.info('Loading analogy questions')
    with codecs.open(Y_base_dir + 'analogy\\analogy.txt', 'r', 'utf-8') as f:
        questions = [line.strip().split(' ') for line in f]

    settings.logger.info('Loading Y...')
    Y = WordEmbeddings.WordEmbeddings(Y_dir + 'Y.' + s_dim, name = 'Y' + s_dim, words_filename = W_dir + 'words.txt', normalize = True)
    models = [Y]
    for filename in W_filenames:
        settings.logger.info('Loading from ' + filename + ' ...')
        W = WordEmbeddings.WordEmbeddings(W_dir + filename, name = filename, words_filename = W_dir + 'words.txt', normalize = True)
        models.append(W)

    settings.logger.info('Testing analogies hit at 1')
    tester1 = AnalogyTester.AnalogyTester(hit_at_n = 1, ignore_question_words = True)
    tester1.run(models, questions, Y_dir + 'analogy_tests_1_new.txt')

    settings.logger.info('Testing analogies hit at 10')
    tester10 = AnalogyTester.AnalogyTester(hit_at_n = 10, ignore_question_words = True)
    tester10.run(models, questions, Y_dir + 'analogy_tests_10_new.txt')

    settings.logging.info('Done')