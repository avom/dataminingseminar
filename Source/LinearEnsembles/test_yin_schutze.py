import codecs
import logging

import AnalogyTester
import settings
import SimilarityTester
import WordEmbeddings

def add_file_logging(dir):
    log_file_name = dir + 'log.txt'

    logger_formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
    logger_filehandler = logging.FileHandler(log_file_name)
    logger_filehandler.setFormatter(logger_formatter)
    settings.logger.addHandler(logger_filehandler)

def run_tests(models, questions_filename, results_filename, case_sensitive = False):
    settings.logger.info('Loading analogy questions from ' + questions_filename)
    with codecs.open(analogy_test_dir + questions_filename, 'r', 'utf-8') as f:
        if case_sensitive:
            questions = [line.strip().split(' ') for line in f]
        else:
            questions = [line.lower().strip().split(' ') for line in f]

    settings.logger.info('Testing analogies hit at 1')
    tester = AnalogyTester.AnalogyTester(hit_at_n = 1, ignore_question_words = True)
    tester.run(models, questions, analogy_test_dir + results_filename)

def run_simlex999(models, questions_filename, case_sensitive = False, similarity_column = 3, header = True):
    settings.logger.info('Loading SimLex-999 questions from ' + questions_filename)
    questions = []
    ignore_header = header
    with codecs.open(questions_filename, 'r', 'utf-8') as f:
        for line in f:
            if ignore_header:
                ignore_header = False
                continue

            if case_sensitive:
                tokens = line.strip().split('\t')
            else:
                tokens = line.lower().strip().split('\t')
            questions.append((tokens[0], tokens[1], float(tokens[similarity_column])))
    
    tester = SimilarityTester.SimilarityTester()
    for model in models:
        settings.logger.info('Running SimLex-999 on ' + model.name)
        tester.run(questions, model)

if __name__ == "__main__":

    work_dir = 'f:\\Projects\\DMS\\data\\yin_schutze\\'
    analogy_test_dir = work_dir + 'analogy_tests\\'
    add_file_logging(work_dir)

    settings.logger.info('Loading models...')
    settings.logger.info('Loading hlbl...')
    hlbl = WordEmbeddings.WordEmbeddings(work_dir + 'hlbl_100', 'HLBL', normalize = True)

    settings.logger.info('Loading huang...')
    huang = WordEmbeddings.WordEmbeddings(work_dir + 'huang_50', 'Huang', normalize = True)

    settings.logger.info('Loading glove...')
    glove = WordEmbeddings.WordEmbeddings(work_dir + 'glove_300', 'Glove', normalize = False)
    glove.vertical_normalize()
    glove.normalize()

    settings.logger.info('Loading cw...')
    cw = WordEmbeddings.WordEmbeddings(work_dir + 'CW_200', 'CW', normalize = True)

    settings.logger.info('Loading w2v...')
    w2v = WordEmbeddings.WordEmbeddings(work_dir + 'word2vec_300', 'W2V', normalize = True)

    settings.logger.info('Loading Y...')
    Y = WordEmbeddings.WordEmbeddings(work_dir + 'Y', 'Y', normalize = True)

    settings.logger.info('Loading X...')
    X = WordEmbeddings.WordEmbeddings(work_dir + 'X', 'X', normalize = True)

    models = [hlbl, huang, glove, cw, w2v, Y, X]
    #run_simlex999(models, work_dir + 'SimLex-999\\SimLex-999.txt', similarity_column = 2)
    run_simlex999(models, work_dir + 'rw\\rw.txt', similarity_column = 2, header = False)
    #run_simlex999(models, work_dir + 'wordsim353\\combined.tab', similarity_column = 2)
    #run_simlex999([Y], work_dir + 'SimLex-999\\SimLex-999.txt')
    #run_tests(models, 'questions-words-sem.txt', 'questions-words-sem-results-y.txt', case_sensitive = False)
    #run_tests(models, 'questions-words-syn.txt', 'questions-words-syn-results-y.txt', case_sensitive = False)

    #models = [w2v]
    #run_tests(models, 'questions-words-sem.txt', 'questions-words-sem-results-w2v.txt', case_sensitive = True)
    #run_tests(models, 'questions-words-syn.txt', 'questions-words-syn-results-w2v.txt', case_sensitive = True)
