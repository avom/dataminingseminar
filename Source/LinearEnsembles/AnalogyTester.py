import codecs
import numpy as np

import settings

class AnalogyTester:
    def __init__(self, hit_at_n = 1, ignore_question_words = False):
        self.hit_at_n = hit_at_n
        self.ignore_question_words = ignore_question_words

    def run(self, models, questions, results_filename):
        self.check_normalized_statuses(models)
        self.test_count = 0
        with codecs.open(results_filename, 'w', 'utf-8', buffering = 1) as outf:
            for question in questions:
                self.test_count += 1
                self.run_test(models, question, outf)

    def run_test(self, models, question, outf):
        settings.logger.info('Running test %d: %s' % (self.test_count, question))
        word_text = ''
        for word in question:
            if word_text != '':
                word_text += '\t'
            word_text += word

        test_result_text = ''
        rank_text = ''
        for emb in models:
            if test_result_text != '':
                test_result_text += '\t'
                rank_text += '\t'

            if not emb.contains_words(question):
                test_result_text += 'NA'
                rank_text += 'NA'
                continue
            i = [emb.index[word] for word in question]
            is_analogy, rank = self.check_analogy(emb.W, i[0], i[1], i[2], i[3], return_rank = True)
            if is_analogy:
                test_result_text += 'TRUE'
            else:
                test_result_text += 'FALSE'
            rank_text += str(rank)
        outf.write(word_text + '\t' + test_result_text + '\t' + rank_text + '\n')
            
    def check_analogy(self, A, i0, i1, i2, i3, return_rank = False):
        if self.embeddings_normalized:
            cos0 = self.cosine_similarities_normalized(A, A[i0])
            cos1 = self.cosine_similarities_normalized(A, A[i1])
            cos2 = self.cosine_similarities_normalized(A, A[i2])
            sum_cos = cos1 - cos0 + cos2
        else:
            b = A[i0] + A[i1] + A[i2]
            sum_cos = self.cosine_similarities(A, b)
        top_indices = sum_cos.argsort()
        if return_rank:
            index = np.where(top_indices == i3)[0][0]
            rank = len(top_indices) - index
        top_indices = top_indices[-self.hit_at_n - 3::]
        if self.ignore_question_words:
            top_indices = top_indices[(top_indices != i0) & (top_indices != i1) & (top_indices != i2)]
        top_indices = top_indices[-self.hit_at_n::]
        if return_rank:
            return i3 in top_indices, rank
        return i3 in top_indices

    def cosine_similarities_normalized(self, A, b):
        return np.ravel(A.dot(b.T))

    def cosine_similarities(self, A, b):
        b_length = np.sqrt(np.sum(b * b))
        A_lengths = np.sqrt((np.multiply(A, A)).sum(axis = 1))
        return A.dot(b) / A_lengths / b_length

    def check_normalized_statuses(self, models):
        self.embeddings_normalized = True
        for embeddings in models:
            settings.logger.info(embeddings.name + ' is ' + ('' if embeddings.normalized else 'not') + ' normalized')
            if not embeddings.normalized:
                self.embeddings_normalized = False