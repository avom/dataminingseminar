import glob2
import os
import sys
import unittest

def create_test_suite():
    test_file_strings = glob2.glob('*Tests.py')
    module_strings = [str[0:-3] for str in test_file_strings]
    suites = [unittest.defaultTestLoader.loadTestsFromName(name) \
              for name in module_strings]
    testSuite = unittest.TestSuite(suites)
    return testSuite

if __name__ == '__main__':
    sys.path.insert(0, os.path.dirname(__file__) + '\\..')
    testSuite = create_test_suite()
    text_runner = unittest.TextTestRunner().run(testSuite)