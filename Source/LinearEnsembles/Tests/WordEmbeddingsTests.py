import numpy as np
import os
import unittest

import WordEmbeddings

class WordEmbeddingsTests(unittest.TestCase):
    def setUp(self):
        if os.path.exists('TestFiles\\embeddings.npy'):
            os.remove('TestFiles\\embeddings.npy')
        if os.path.exists('TestFiles\\embeddings_words.txt'):
            os.remove('TestFiles\\embeddings_words.txt')
        return super().setUp()

    def tearDown(self):
        if os.path.exists('TestFiles\\embeddings.npy'):
            os.remove('TestFiles\\embeddings.npy')
        if os.path.exists('TestFiles\\embeddings_words.txt'):
            os.remove('TestFiles\\embeddings_words.txt')
        return super().tearDown()

    def test_embeddingsFromTextFile(self):
        emb = WordEmbeddings.WordEmbeddings('TestFiles\\embeddings', normalize = False)

        self.assertEquals(['c', 'a', 'b', 'd'], emb.words)
        expected_W = np.array([[0.1, 0.4], [-1.0, 0.9], [0.7, -0.2], [0.3, -0.9]])
        self.assertTrue(np.allclose(expected_W, emb.W))
        self.assertEquals([1, 2, 0, 3], [emb.index[i] for i in ['a', 'b', 'c', 'd']])

    def test_normalize(self):
        emb = WordEmbeddings.WordEmbeddings()
        W = np.matrix([[0.1, -0.2], [0.3, 0.4], [-0.5, -0.6]])
        words = ['b', 'a', 'x']
        emb.set_embeddings(W.copy(), words.copy())

        emb.normalize()

        W_normalized = np.matrix([
            [0.1 / 0.223607, -0.2 / 0.223607], 
            [0.3 / 0.5, 0.4 / 0.5], 
            [-0.5 / 0.781025, -0.6 / 0.781025]])
        self.assertTrue(emb.normalized)
        self.assertTrue(np.allclose(W_normalized, emb.W))

    def test_vertical_normalize(self):
        emb = WordEmbeddings.WordEmbeddings()
        W = np.matrix([[0.1, -0.2], [0.3, 0.4], [-0.5, -0.6]])
        words = ['b', 'a', 'x']
        emb.set_embeddings(W.copy(), words.copy())

        emb.vertical_normalize()

        W_vert_normalized = np.matrix([
            [0.1 / 0.591608, -0.2 / 0.748331], 
            [0.3 / 0.591608, 0.4 / 0.748331], 
            [-0.5 / 0.591608, -0.6 / 0.748331]])
        self.assertTrue(np.allclose(W_vert_normalized, emb.W))

    def test_set_embeddings(self):
        emb = WordEmbeddings.WordEmbeddings()
        W = np.matrix([[0.1, -0.2], [0.3, 0.4], [-0.5, -0.6]])
        words = ['b', 'a', 'x']

        emb.set_embeddings(W.copy(), words.copy())

        self.assertEquals(words, emb.words)
        self.assertTrue(np.allclose(W, emb.W))
        self.assertEquals([0, 1, 2], [emb.index[i] for i in words])

    def test_WP(self):
        emb = WordEmbeddings.WordEmbeddings()
        W = np.matrix([[0.1, -0.2], [0.3, 0.4], [-0.5, -0.6]])
        words = ['b', 'a', 'x']
        emb.set_embeddings(W.copy(), words.copy())
        emb.P = np.matrix([[2, 3], [4, 5]])
        
        WP = emb.WP()

        WP_expected = np.matrix([
            [0.1 * 2 - 0.2 * 4, 0.1 * 3 - 0.2 * 5], 
            [0.3 * 2 + 0.4 * 4, 0.3 * 3 + 0.4 * 5],
            [-0.5 * 2 - 0.6 * 4, -0.5 * 3 - 0.6 * 5]])
        self.assertTrue(np.allclose(WP_expected, WP))

    def test_contains_words(self):
        emb = WordEmbeddings.WordEmbeddings()
        W = np.matrix([[0.1, -0.2], [0.3, 0.4], [-0.5, -0.6]])
        words = ['b', 'a', 'x']
        emb.set_embeddings(W.copy(), words.copy())

        returnValue = emb.contains_words(['a', 'x', 'b'])
        self.assertTrue(returnValue, 'All words are present in the model')

        returnValue = emb.contains_words(['a', 'c', 'b'])
        self.assertFalse(returnValue, '"c" is missing in the model')

if __name__ == '__main__':
    unittest.main()
