import numpy as np
import unittest

import AnalogyTester

class AnalogyTesterTests(unittest.TestCase):

    def test_cosine_similarities(self):
        A = np.array([
            [0.4, 0.5],
            [-0.1, 0.8],
            [-0.4, -0.7]])
        b = A[1]
        sut = AnalogyTester.AnalogyTester()
        sut.embeddings_normalized = False
        returnValue = sut.cosine_similarities(A, b)
        
        expected = np.array([0.697355, 1, -0.8])
        self.assertTrue(np.allclose(expected, returnValue))

    def test_cosine_similarities_normalized(self):
        A = np.matrix([                 # L2-normalized
            [0.624695, 0.780869],       # 0.4, 0.5
            [-0.124035, 0.992278],      # -0.1, 0.8
            [-0.496139, -0.868243]])    # -0.4, -0.7
        b = A[1]
        sut = AnalogyTester.AnalogyTester()
        sut.embeddings_normalized = True
        returnValue = sut.cosine_similarities_normalized(A, b)
        
        expected = np.array([0.697355, 1, -0.8])
        self.assertTrue(np.allclose(expected, returnValue))

    def test_check_analogy_return_true(self):
        A = np.matrix([
            [1.0, 0.0],
            [0.0, 1.0],
            [0.8, 0.6],
            [-0.8, 0.6],
            [-0.1240347, 0.9922779]])
        sut = AnalogyTester.AnalogyTester()
        sut.embeddings_normalized = True
        returnValue = sut.check_analogy(A, 0, 1, 2, 4)
        self.assertTrue(returnValue)

    def test_check_analogy_returns_false(self):
        A = np.matrix([
            [1.0, 0.0],
            [0.0, 1.0],
            [0.8, 0.6],
            [-0.8, 0.6],
            [-0.1240347, 0.9922779]])
        sut = AnalogyTester.AnalogyTester()
        sut.embeddings_normalized = True
        returnValue = sut.check_analogy(A, 0, 1, 2, 3)
        self.assertFalse(returnValue)

    def test_check_analogy_returns_ignore_question_words(self):
        A = np.matrix([
            [1.0, 0.0],
            [0.0, 1.0],
            [0.8, 0.6],
            [-0.8, 0.6],
            [0.1240347, 0.9922779]])
        sut = AnalogyTester.AnalogyTester(ignore_question_words = True)
        sut.embeddings_normalized = True
        returnValue = sut.check_analogy(A, 0, 1, 2, 4)
        self.assertTrue(returnValue)

    def test_check_analogy_returns_in_top_3(self):
        A = np.matrix([
            [1.0, 0.0],
            [0.0, 1.0],
            [0.8, 0.6],
            [-0.8, 0.6],
            [0.1240347, 0.9922779],
            [-0.1240347, 0.9922779],
            [-0.1240347, -0.9922779]])
        sut = AnalogyTester.AnalogyTester(hit_at_n = 3)
        sut.embeddings_normalized = True
        returnValue = sut.check_analogy(A, 0, 1, 2, 4)
        self.assertTrue(returnValue)
