import codecs
import gensim
import os
import numpy as np

import settings

class WordEmbeddings:
    def __init__(self, filename = '', name = '', words_filename = None, is_w2v = False, keep_case = True, normalize = False):
        self.normalized = normalize
        self.words_filename = words_filename if words_filename is not None else filename + '_words.txt'
        if filename != '':
            if os.path.isfile(filename + '.npy') and os.path.isfile(self.words_filename):
                self.load_existing(filename)
            elif is_w2v:
                self.load_from_word2vec(filename)
            else:
                self.load_from_text(filename)
        else:
            self.words = []
        self.name = name
        self.index = {value: index for index, value in enumerate(self.words)}

        self.index_lower = dict()
        for i in range(len(self.words)):
            word = self.words[i].lower()
            if not (word in self.index_lower):
                self.index_lower[word] = i

    def load_from_text(self, filename):
        self.extract_words(filename)

    def extract_words(self, filename):
        self.words = []
        vocabulary_size = self.count_lines(filename + '.txt')
        self.W = None
        with codecs.open(filename + '.txt', 'r', 'utf-8') as inf:
            with codecs.open(self.words_filename, 'w', 'utf-8', buffering = 0) as outf:
                word_count = 0
                for line in inf:
                    tokens = line.strip().split(' ')
                    tokens = list(filter(None, tokens))
                    if self.W is None:
                        dims = len(tokens) - 1
                        self.W = np.zeros((vocabulary_size, dims))
                    assert len(tokens) - 1 == dims
                    word = tokens[0].lower()
                    self.words.append(word)
                    outf.write(word + '\n');

                    for i in range(1, len(tokens)):
                        self.W[word_count, i - 1] = float(tokens[i])

                    word_count += 1
                    if word_count % 10000 == 0:
                        settings.logger.info('%d words extracted' % word_count)
        if self.normalized:
            self.normalize()
        np.save(filename + '.npy', self.W, allow_pickle = False)

    def count_lines(self, filename):
        with codecs.open(filename, 'r', 'utf-8', buffering = 0) as f:
            result = 0
            for line in f:
                result += 1
        return result

    def load_from_word2vec(self, filename):
        model = gensim.models.Word2Vec.load_word2vec_format(filename + '.bin', binary = True)
        words = [word for word in model.vocab.keys() if not ('_' in word)]
        words.sort(key = lambda w: -model.vocab[w].count)
        self.words = [word for word in words]

        rows = len(words)
        self.W = np.array([model[words[i]] for i in range(rows)])
        if self.normalized:
            self.normalize()
        np.save(filename + '.npy', self.W, allow_pickle = False)

        with codecs.open(self.words_filename, 'w', 'utf-8', buffering = 0) as outf:
            for word in words:
                outf.write(word + '\n')

    def load_existing(self, filename):
        with codecs.open(self.words_filename, 'r', 'utf-8') as f:
            self.words = f.read().splitlines()
        self.W = np.load(filename + '.npy')
        if self.normalized:
            self.normalize()

    def extend(self, new_dims):
        old_dims = self.dims()
        if new_dims == old_dims:
            return
        assert new_dims > old_dims, 'Throwing away extra dimensions is not implemented'
        self.origW = self.W
        self.W = np.zeros((self.W.shape[0], new_dims))
        self.W[:,:old_dims - new_dims] = self.origW

    def dims(self):
        return self.W.shape[1]

    def WP(self):
        return self.W.dot(self.P)

    def normalize(self):
        L2norm = np.sqrt((np.multiply(self.W, self.W)).sum(axis = 1))
        self.W = self.W / L2norm.reshape(L2norm.shape[0], 1)
        self.normalized = True

    def vertical_normalize(self):
        L2norm = np.sqrt((np.multiply(self.W, self.W)).sum(axis = 0))
        self.W = self.W / L2norm

    def set_embeddings(self, W, words):
        assert W.shape[0] == len(words), 'Mismatch in number of rows in W and number of words'
        self.W = W
        self.words = words
        self.index = {value: index for index, value in enumerate(self.words)}
        self.normalized = False

    def contains_words(self, words):
        for word in words:
            if not (word in self.index):# and not (word in self.index_lower):
                return False
        return True
