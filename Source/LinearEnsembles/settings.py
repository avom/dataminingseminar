import logging
import os.path
import sys

program = os.path.basename(sys.argv[0])
logger = logging.getLogger(program)

class LogEater(object):
    def write(self):
       pass
logging_disabled = sum([sys.argv[i] == '-disable-logging' for i in range(len(sys.argv))]) > 0

logging.basicConfig(
    format  ='%(asctime)s: %(levelname)s: %(message)s', 
    stream = sys.stdout if not logging_disabled else LogEater)
logging.root.setLevel(level = logging.INFO)
