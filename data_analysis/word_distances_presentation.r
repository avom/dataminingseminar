data <- word.distances("procrustes", 50)
data <- data[order(-data$mean.d2),]
head(data, 100)

data <- word.distances("procrustes", 100)
data <- data[order(-data$mean.d2),]
head(data, 100)


data <- word.distances("linear", 300)
data <- data[order(-data$mean.d2),]
head(data, 100)



for (dimensions in 1:6 * 50) {
  for (method in c("linear", "procrustes")) {
    data <- word.distances(method, dimensions)
    data <- data[order(-data$mean.d2),]
    data <- data[1:1000,]
    write.table(data, paste0("word_distances_", method, dimensions, ".csv"), quote = F)
  }
}
