setwd("c:\\Projects\\DataMiningSeminar\\data\\nodalida\\")
setwd("c:\\Projects\\DataMiningSeminar\\data\\alligaator\\procrustes50\\")

###### Synonym tests

method = ""
method = "noisy_sopp"
cols = 4:13
test.synonyms <- function(method, cols = 2:3) {
  filename <- paste0("synonym_ranks", ifelse(method != "", "_", ""), method, ".txt")
  data <- read.csv(filename, sep = "\t", header = F)
  colnames(data) <- c("Word1", "Word2", "Y", paste0("W", 0:(length(cols) - 1)))
  
  data$meanW <- rowMeans(data[, cols])
  
  print(paste0("Method: ", method))
  print(paste0("Mean Y: ", mean(data$Y)))
  print(paste0("Mean W: ", mean(data$meanW)))
  print(paste0("Var Y: ", var(data$Y)))
  print(paste0("Var W: ", var(data$meanW)))
  print(wilcox.test(data$Y, data$meanW, paired = TRUE))
  return(data)
}


test.synonyms("slrce")
test.synonyms("sopp")

data = test.synonyms("", 4:13)
colMeans(data[, 4:13])

colnames(data)

data = test.synonyms("noisy_sopp", 4:13)
colMeans(data[, 4:13])

######### Analogy tests

analog.accuracies.total <- function(method, dimensions, top = 1, file.name = "analogy_results") {
  hits <- analogy.hits(method, dimensions, top, file.name)
  total <- hits[1]
  Y <- hits[2]
  W.hits <- tail(hits, 10)
  W <- mean(W.hits)
  W.min <- min(W.hits)
  W.max <- max(W.hits)
  result <- c(Y, W, W.min, W.max) / total
  result <- c(result, conf.interval(W.hits / total))
  names(result) <- c("Y", "W", "W.min", "W.max", "W.low", "W.high", "Shapiro.p")
  return(result)
}


conf.interval <- function(x, alpha = 0.05) {
  n <- length(x)
  mean.x <- mean(x)
  sd.x <- sd(x)
  error <- qnorm(1 - alpha / 2) * sd.x / sqrt(n)
  return (c(mean.x - error, mean.x + error, shapiro.test(x)$p.value))
}

filename <- "analogy_test_results_top_0.txt"
  
load.data <- function(filename, col.names) {
  data <- read.csv(filename, sep = "\t", header = F)
  for (i in 2:ncol(data))
    data[, i] <- as.character(levels(data[, i])[data[, i]]) == "True"
  return(data)
}

analogy.hits <- function(data) {
  hits <- colSums(data[, 2:ncol(data)]) 
  return(c(nrow(data), hits))
}

all.hits = data.frame()
matrix.names = c("Y_slrce", "Y_sopp", "est", "etwiki") 
for (i in 10^(0:2)) {
  filename = paste0("analogy_test_results_top_", i, ".txt")
  hits <- analogy.hits(load.data(filename))
  all.hits <- rbind(all.hits, c(i, hits))
}
colnames(all.hits) <- c("top", "tests", matrix.names)

accuracies = data.frame(top = all.hits$top)
accuracies$Y_slrce = all.hits$Y_slrce / all.hits$tests
accuracies$Y_sopp = all.hits$Y_sopp / all.hits$tests
accuracies$est = all.hits$est / all.hits$tests
accuracies$etwiki = all.hits$etwiki / all.hits$tests

all.hits
accuracies

data1 = load.data("analogy_results1.txt")
data1 = load.data("analogy_results10.txt")

data1 = load.data("analogy_test_results_noisy_top_1.txt")
data1 = load.data("analogy_test_results_noisy_top_10.txt")
colnames(data1) = c("Test", "Y", paste0("W", 0:9))
colSums(data1[, 2:12])
colMeans(data1[,2:12])
head(data1)

all.hits$combined <- c(sum(load.data("analogy_test_results_noisy_top_1.txt")[,2]), 
                       sum(load.data("analogy_test_results_noisy_top_10.txt")[,2]))

all.hits$combined <- c(sum(load.data("analogy_test_results_top_1_combined.txt")[,2]), 
                       sum(load.data("analogy_test_results_top_10_combined.txt")[,2]), 
                       sum(load.data("analogy_test_results_top_100_combined.txt")[,2]))

accuracies$combined = all.hits$combined / all.hits$tests
