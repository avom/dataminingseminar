setwd("C:\\Projects\\DataMiningSeminar\\data\\alligaator\\")

# method <- "linear"
# method <- "procrustes"
# dimensions <- 100
# 
# data.linear <- data
# data.procrustes <- data
# 
# head(data.procrustes)
# head(data.linear)

sims.plot <- function(method, dimensions, sample.size = NA, font.size = 1) {
  set.seed(0)
  filename <- ifelse(method == "SLRCE", "linear", "procrustes")
  filename <- paste0(filename, dimensions, "\\random_similarities.txt")
  data <- read.csv(filename, sep = "\t", header = F)
  if (!is.na(sample.size))
    data <- data[sample(nrow(data), sample.size), ]
  colnames(data) <- c("Word1", "Word2", "Y", paste0("W", 0:9))
  
  data$meanW <- rowMeans(data[, 4:13])
  data$minW <- apply(data[, 4:13], 1, min)
  data$maxW <- apply(data[, 4:13], 1, max)
  
  data <- data[order(data$Y),]
  
  plot(data$Y, 
       type = "l", 
       ylim = c(-1, 1), 
       xlab = "Word pairs", 
       ylab = "Similarity", 
       main = paste0(method, " ", dimensions), 
       cex.main = font.size, 
       cex.lab = font.size, 
       cex.axis = font.size
      )
  lines(data$minW, col = "blue")
  lines(data$maxW, col = "red")
  lines(data$meanW, col = "green")
  lines(data$Y, col = "black", lwd = 2)
  legend(0, max(c(data$maxW, data$Y)),
         c("similarity in Y", "max similarity", "min similarity", "mean similarity"), 
         lty = c(1, 1),
         lwd = c(2, 2),
         col = c("black", "red", "blue", "green"),
         cex = min(font.size, sqrt(font.size)))
}

#sims.plot("procrustes50\\random_similarities.txt")

png(filename="..\\..\\master\\paper\\images\\random_similarities.png", height = 1400, width = 500)
par(mfrow = c(6, 2))
for (dimensions in 1:6 * 50) {
  for (method in c("SLRCE", "SOPP"))
    sims.plot(method, dimensions)
}
par(mfrow = c(1, 1))
dev.off()


png(filename="..\\..\\master\\paper\\images\\random_similarities.png", height = 500, width = 1000)
par(mfrow = c(1, 2))
for (dimensions in 100) {
  for (method in c("SLRCE", "SOPP"))
    sims.plot(method, dimensions, font.size = 1.7)
}
par(mfrow = c(1, 1))
dev.off()
