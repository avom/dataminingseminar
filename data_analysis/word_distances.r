setwd("c:\\Projects\\DataMiningSeminar\\data\\alligaator\\")

library(data.table)

method <- "linear"
dim <- 50

word.distances <- function(method, dim, sample.size = 0) {
  if (method == "SLRCE")
    method <- "linear"
  else if (method == "SOPP")
    method <- "procrustes"
  filename <- paste0(method, dim, "\\word_distances.txt")
  df <- fread(filename, sep = "\t", header = F, verbose = TRUE, encoding = "UTF-8")
  colnames(df) <- c("word", "index", "mean.d2")
  if (sample.size > 0)
    df <- df[sample(nrow(df), sample.size), ]
  return(df)
}

word.distances.plot <- function(methods, dims, sample.size = 0, filename = NA, ylim = NA) {
  if (!is.na(filename))
    png(filename = paste0("..\\..\\master\\paper\\images\\", filename, ".png"), height = min(1400, 400 * length(dims)), width = 250 * length(methods))
  par(mfrow = c(length(dims), length(methods)))
  dfs <- list()
  for (dim in dims) {
    ymax <- 0
    for (method in methods) {
      df <- word.distances(method, dim, sample.size)
      ymax <- max(df$mean.d2)
      dfs[[method]] <- df
    }
    for (method in methods) {
      df <- dfs[[method]]
      plot(df$index, 
           df$mean.d2, 
           pch = 16, 
           main = paste0(method, " (", dim, " dimensions)"), 
           xlab = "Most popular words first", 
           ylab = "Mean squared distance",
           ylim = c(0, ymax))
    }
  }
  par(mfrow = c(1, 1))
  if (!is.na(filename))
    dev.off()
}

methods <- c("SLRCE", "SOPP")
dimensions <- 1:6 * 50
word.distances.plot(methods, dimensions)

word.distances.plot(methods, 100)
word.distances.plot(methods, 100, filename = "word_distances_100")
word.distances.plot(methods, 100, sample.size = 1000)
set.seed(4)
word.distances.plot(methods, 100, filename = "word_distances_100_sample", sample.size = 1000)

