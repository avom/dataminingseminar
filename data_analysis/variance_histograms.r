setwd("c:\\Projects\\DataMiningSeminar\\master\\data_analysis\\")

plot.hist <- function(method, dimensions, max.var = NA) {
  filename <- paste0("..\\..\\data\\alligaator\\", 
                     tolower(method), dimensions, 
                     "\\mean_vars_", dimensions, ".txt")
  data <- read.table(filename)$V1
  if (!is.na(max.var)) {
    data <- data[data <= max.var]
  }
  hist(data, main = paste(method, dimensions), xlab = "Variance")
}

png(filename="..\\paper\\images\\variance_histograms.png", height = 1400, width = 500)
par(mfrow = c(6, 2))
for (dimensions in 50 * 1:6) {
  plot.hist("Procrustes", dimensions)
  plot.hist("Linear", dimensions)
}
par(mfrow = c(1, 1))
dev.off()

png(filename="..\\paper\\images\\variance_histograms_limited.png", height = 1400, width = 500)
par(mfrow = c(6, 2))
for (dimensions in 50 * 1:6) {
  plot.hist("Procrustes", dimensions, 0.1)
  plot.hist("Linear", dimensions, 0.1)
}
par(mfrow = c(1, 1))
dev.off()