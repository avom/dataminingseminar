setwd("c:\\Projects\\DataMiningSeminar\\data\\alligaator\\")
#setwd("c:\\School\\DataMiningSeminar\\data\\alligaator\\")

method <- "linear"
dimensions <- 50
top <- 1
file.name = "analogy_results"

conf.interval <- function(x, alpha = 0.05) {
  n <- length(x)
  mean.x <- mean(x)
  sd.x <- sd(x)
  error <- qnorm(1 - alpha / 2) * sd.x / sqrt(n)
  return (c(mean.x - error, mean.x + error, shapiro.test(x)$p.value))
}


load.data <- function(method, dimensions, top = 1, file.name = "analogy_results") {
  filename <- paste0(method, dimensions, "\\", file.name, top, ".txt")
  data <- read.csv(filename, sep = "\t", header = F)
  colnames(data) <- c("words", "Y", paste0("W", 0:9))
  for (i in 2:ncol(data))
    data[, i] <- as.character(levels(data[, i])[data[, i]]) == "True"
  return(data)
}

analog.hits <- function(method, dimensions, top = 1, file.name = "analogy_results") {
  data <- load.data(method, dimensions, top, file.name)
  hits <- colSums(data[, 2:ncol(data)]) 
  return(c(nrow(data), hits))
}

analog.accuracies <- function(method, dimensions) {
  hits <- analog.hits(method, dimensions, file.name)
  return(hits / hits[1])
}

analog.accuracies.total <- function(method, dimensions, top = 1, file.name = "analogy_results") {
    hits <- analog.hits(method, dimensions, top, file.name)
    total <- hits[1]
    Y <- hits[2]
    W.hits <- tail(hits, 10)
    W <- mean(W.hits)
    W.min <- min(W.hits)
    W.max <- max(W.hits)
    result <- c(Y, W, W.min, W.max) / total
    result <- c(result, conf.interval(W.hits / total))
    names(result) <- c("Y", "W", "W.min", "W.max", "W.low", "W.high", "Shapiro.p")
    return(result)
}

all.same <- function(method, dimensions) {
  data <- load.data(method, dimensions)
  for (i in 3:ncol(data)) {
    if (!all.equal(data[, i - 1], data[, i]))
      return(FALSE)
  }
  return(TRUE)
}
# 
# data <- load.data("linear", 50)
# 
# 
# for (dimensions in 1:6 * 50) {
#   for (method in c("linear", "procrustes")) {
#     print(paste0(method, dimensions, ": "))
#     print(analog.hits(method, dimensions))
#     print(analog.accuracies(method, dimensions))
#     #print(all.same(method, dimensions))
#   }
# }

analog.accuracies.total("procrustes", 300, 10, "analogy_test_results_noisy_top_")
analog.accuracies.total("linear", 300, 10, "analogy_test_results_noisy_top_")

# top = 1 ###############################################################################################################################

linear <- c()
procrustes <- c()
dimensions <- 1:6 * 50

for (dim in dimensions) {
  linear <- c(linear, analog.accuracies.total("linear", dim))
  procrustes <- c(procrustes, analog.accuracies.total("procrustes", dim))
}

linear <- round(linear, 3)
procrustes <- round(procrustes, 3)
data.frame(dimensions, 
           linear = linear[c(TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)], 
           procrustes = procrustes[c(TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)], 
           Initial.mean = procrustes[c(FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE)],
           Initial.min = procrustes[c(FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE)],
           Initial.max = procrustes[c(FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE)],
           Conf.low = procrustes[c(FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)],
           Conf.high = procrustes[c(FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE)],
           Shapiro.p = procrustes[c(FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)]
          )

# top = 10 ##############################################################################################################################

linear <- c()
procrustes <- c()
dimensions <- 1:6 * 50
top <- 10

for (dim in dimensions) {
  linear <- c(linear, analog.accuracies.total("linear", dim, top))
  procrustes <- c(procrustes, analog.accuracies.total("procrustes", dim, top))
}

linear <- round(linear, 3)
procrustes <- round(procrustes, 3)
data.frame(dimensions, 
           linear = linear[c(TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)], 
           procrustes = procrustes[c(TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)], 
           Initial.mean = procrustes[c(FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE)],
           Initial.min = procrustes[c(FALSE, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE)],
           Initial.max = procrustes[c(FALSE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE)],
           Conf.low = procrustes[c(FALSE, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)],
           Conf.high = procrustes[c(FALSE, FALSE, FALSE, FALSE, FALSE, TRUE, FALSE)],
           Shapiro.p = procrustes[c(FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)]
          )

# top = 1 multiplicative ################################################################################################################

linear <- c()
procrustes <- c()
dimensions <- 1:6 * 50
top <- 1
file.name <- "analogy_results_mul"

for (dim in dimensions) {
  linear <- c(linear, analog.accuracies.total("linear", dim, top, file.name))
  procrustes <- c(procrustes, analog.accuracies.total("procrustes", dim, top, file.name))
}

linear <- round(linear, 3)
procrustes <- round(procrustes, 3)
data.frame(dimensions, linear = linear[c(TRUE, FALSE)], procrustes = procrustes[c(TRUE, FALSE)], Initial = procrustes[c(FALSE, TRUE)])


# top = 10 multiplicative ###############################################################################################################

linear <- c()
procrustes <- c()
dimensions <- 1:6 * 50
top <- 10

for (dim in dimensions) {
  linear <- c(linear, analog.accuracies.total("linear", dim, top, file.name))
  procrustes <- c(procrustes, analog.accuracies.total("procrustes", dim, top, file.name))
}

linear <- round(linear, 3)
procrustes <- round(procrustes, 3)
data.frame(dimensions, linear = linear[c(TRUE, FALSE)], procrustes = procrustes[c(TRUE, FALSE)], Initial = procrustes[c(FALSE, TRUE)])

