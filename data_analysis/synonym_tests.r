setwd("c:\\Projects\\DataMiningSeminar\\data\\alligaator\\")

method = "procrustes"
dimensions = 50
filename = "synonym_ranks_noisy_sopp.txt"

test.synonyms <- function(method, dimensions, filename = "synonym_ranks.txt") {
  filename <- paste0(method, dimensions, "\\", filename)
  data <- read.csv(filename, sep = "\t", header = F)
  colnames(data) <- c("Word1", "Word2", "Y", paste0("W", 0:9))

  data$meanW <- rowMeans(data[, 4:13])
  
  print(paste0("Method: ", method, ", dimensions: ", dimensions))
  print(paste0("Mean Y: ", mean(data$Y)))
  print(paste0("Mean W: ", mean(data$meanW)))
  print(paste0("Var Y: ", var(data$Y)))
  print(paste0("Var W: ", var(data$meanW)))
  print(wilcox.test(data$Y, data$meanW, paired = TRUE))
}


for (dimensions in 1:6 * 50) {
  for (method in c("linear", "procrustes")) {
    test.synonyms(method, dimensions)
  }
}

test.synonyms("procrustes", 50, "synonym_ranks_noisy_sopp.txt")
test.synonyms("procrustes", 50, "synonym_ranks_tr_sopp.txt")

head(data)
