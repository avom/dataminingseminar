options(scipen = 999)

sols.ranks = read.csv('synonym_ranks_sols.txt', sep = "\t", header = F)
sopp.ranks = read.csv('synonym_ranks_sopp.txt', sep = "\t", header = F)

breaks = c(1, 10, 100, 1000, 10000, 100000, 816757)
sols.cnts = hist(sols.ranks$V3, breaks = breaks)$counts
sopp.cnts = hist(sopp.ranks$V3, breaks = breaks)$counts
W0.cnts = hist(sopp.ranks$V4, breaks = breaks)$counts
print(sopp.cnts)
print(W0.cnts)

lo = head(breaks, -1) + c(0, rep(1, length(breaks) - 2))
hi = tail(breaks, -1)
breaks.str = paste0(lo, " - ", hi)

names(sols.cnts) = breaks.str
names(sopp.cnts) = breaks.str
names(W0.cnts) = breaks.str

#png(filename = "synonym_ranks_W0_100.png", width = 500, height = 500)
#barplot(W0.cnts, main = "Synonym ranks (100 dimensions)")
#dev.off()

png(filename = "synonym_ranks_100.png", width = 1500, height = 500)
# par(mfrow = c(1, 2))
barplot(rbind(sopp.cnts, W0.cnts), beside=T, las = 0, col=gray.colors(2), cex.lab=2, cex.axis=2, cex.names=2, cex.sub=1.5)
legend("topright", legend=c("SOPP", "initial W"), fill=gray.colors(2), cex=2, pt.cex=2)
# barplot(sopp.cnts, main = "SOPP 100", las = 2)
# barplot(W0.cnts, main = "W initial 100", las = 2)
# par(mfrow = c(1, 1))
dev.off()
