from multiprocessing.pool import Pool

import codecs
import gensim
import numpy as np
import pathlib
import pickle

import settings

def save_numpy_array(filename, model, words):
    rows = len(words)
    cols = len(model[words[0]])
    arr = np.matrix([model[words[i]] for i in range(rows)])
    np.save(filename, arr, allow_pickle = False)

def extract_matrix_from_model(index, words):
    model_filename = settings.get_w2v_model_filename(index)
    model = gensim.models.Word2Vec.load_word2vec_format(model_filename, binary = True)
    matrix_filename = settings.get_w2v_matrix_filename(index)
    save_numpy_array(matrix_filename, model, words)

def save_words(filename, words):
    with open(filename, "wb") as f:
        pickle.dump(words, f)

if __name__ == "__main__":
    words_file = pathlib.Path(settings.words_filename)
    if words_file.is_file():
        words = settings.get_words()
    else:
        filename0 = settings.get_w2v_model_filename(0)
        model0 = gensim.models.Word2Vec.load_word2vec_format(filename0, binary = True)
        words = list(model0.vocab.keys())
        save_words(settings.words_filename, words)

    with Pool(processes = settings.thread_count) as pool:
        for i in range(settings.model_count):
            pool.apply_async(extract_matrix_from_model, (i, words, ))
        pool.close()
        pool.join()
