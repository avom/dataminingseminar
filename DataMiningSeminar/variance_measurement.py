import procrustes
import settings

import numpy as np

if __name__ == "__main__":
    load_file_dWPi = False
    load_file_vars_dWPi = False

    settings.logger.info("Loading Y...")
    Y = np.load(settings.Y_filename)

    settings.logger.info("Loading WP0, WP1, ...")
    WPi = [np.load(settings.get_WP_filename(i)) for i in range(settings.model_count)]

    settings.logger.info("Generating difference matrices...")
    if load_file_dWPi:
        dWPi = [np.load(settings.get_vars_of_diff_WP_Y_filename(i)) for i in range(settings.model_count)]
    else:
        dWPi = [WP - Y for WP in WPi]
        for i in range(len(dWPi)):
            np.save(settings.get_vars_of_diff_WP_Y_filename(i), dWPi[i], allow_pickle = False)

    settings.logger.info("Calculating variances of differences in WP - Y for each word...")
    if load_file_vars_dWPi:
        vars_dWPi = [np.load(settings.get_diff_of_WP_Y_filename(i)) for i in range(settings.model_count)]
    else:
        vars_dWPi = [np.apply_along_axis(np.var, axis = 1, arr = dWP) for dWP in dWPi]
        for i in range(len(vars_dWPi)):
            np.save(settings.get_diff_of_WP_Y_filename(i), vars_dWPi[i], allow_pickle = False)

    settings.logger.info("Calculating mean variance for each word...")
    def matrix_list_mean(mats):
        if len(mats) == 0:
            return 0
        sum = mats[0].copy()
        for a in mats[1:]:
            sum += a
        return sum / len(mats)

    mean_vars_dWPi = matrix_list_mean(vars_dWPi)

    settings.logger.info("Save mean variances...")
    np.savetxt(settings.mean_variances_filename, mean_vars_dWPi, fmt = "%.19f")
