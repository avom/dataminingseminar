import numpy as np

A = np.matrix([i for i in range(1, 11)]).reshape((5, 2))
B = np.matrix([i for i in range(1, 5 * 3 + 1)]).reshape((5, 3))
print(A)
print(B)

U, Ds, Vt = np.linalg.svd()