from gensim.corpora import MmCorpus, WikiCorpus
from gensim.models.word2vec import BrownCorpus, LineSentence, Word2Vec

import codecs
import pickle
import settings

# from http://stackoverflow.com/q/23735576
def generate_lines(wiki):
    max_sentence = -1
    for index, text in enumerate(wiki.get_texts()):
        if index < max_sentence or max_sentence == -1:
            words = [word.decode('utf-8') for word in text]
            yield [word if word.replace('-', '').isalpha() else 'num' for word in words if word.replace('-', '').isalnum()]
        else:
            break

def get_wiki_corpus_lines(filename):
    wiki = WikiCorpus(settings.nodalida_data_path + 'etwiki\\' + filename)
    #wiki = MmCorpus(settings.nodalida_data_path + "etwiki.mm")
    lines = list(generate_lines(wiki))
    with open(settings.nodalida_data_path + 'etwiki.pickle', 'wb') as f:
        pickle.dump(lines, f)
    return lines

if __name__ == "__main__":
    #lines = [[b'lalalalal', b'123'], [b'aaa', b'bbb', b'c']]
    #with open('lines.pickle', 'wb') as f:
    #    pickle.dump(lines, f)
    #lines = [['lalalalal', '123'], ['aaa', 'bbb', 'c']]
    #with open('lines.txt', 'w') as f:
    #    for sentence in lines:
    #        f.write(' '.join(sentence) + '\n')

    #max_sentence = -1
    #lines = get_wiki_corpus_lines('etwiki-20170101-pages-articles.xml.bz2')

    #with open(settings.nodalida_data_path + 'etwiki.pickle', 'rb') as f:
    #    lines = pickle.load(f)

    #lines = [[w.decode('utf-8') for w in l] for l in lines]
    #with codecs.open(settings.nodalida_data_path + 'lines.txt', 'w', 'utf-8') as f:
    #    for sentence in lines:
    #        f.write(' '.join(sentence) + '\n')

    #model = Word2Vec(lines, sg = 1, size = 300, window = 10, min_count = 5, workers = 8)
    #model.save(settings.nodalida_data_path + 'etwiki.w2v')

    dir = 'f:\\Projects\\DMS\\data\\wiki-en\\'
    for i in range(1, 10):
        sentences = LineSentence(dir + 'full.txt')
        model = Word2Vec(sentences, sg = 1, size = 50, window = 10, min_count = 5, workers = 4)
        model.save(dir + 'wiki-en-%d.w2v' % i)
    settings.logger.info("Done")