import numpy as np

import analogy_tests
import iterative_minimization
import procrustes
import settings
import synonym_comparison

result_folder = 'f:\\Projects\\DMS\\gigaword\\dimtest\\'

def calculateP(Y, Wi):
    if Wi.shape[1] == 100:
        return procrustes.calculateP(Y, Wi)
    S = Wi.T.dot(Y)
    StS = S.T.dot(S)
    SSt = S.dot(S.T)
    V, Ds1, Vt = np.linalg.svd(StS)
    W, Ds2, Wt = np.linalg.svd(SSt)
    D = np.identity(100)[:-50, :]
    T = W.dot(D).dot(Vt)
    return T

if __name__ == '__main__':
   
    settings.logger.info('Loading words')
    words = settings.get_words()

    settings.logger.info('Loading synonyms')
    synonyms = settings.get_synonyms()

    settings.logger.info('Loading W0, W1')
    W0 = np.load(settings.word2vec_models_path + 'w10.s50.0.bin.npy')
    W1 = np.load(settings.word2vec_models_path + 'w10.s100.0.bin.npy')

    settings.logger.info('Creating W00')
    W00 = np.zeros(W1.shape)
    W00[:,:-50] = W0

    #Y1, P1i, WP1i = iterative_minimization.calculateY([W1, W00], calculateP = procrustes.calculateP, max_it = 1)
    Y2, P2i, WP2i = iterative_minimization.calculateY([W1, W0], calculateP = calculateP, max_it = 1)

    synonym_comparison.rank_synonyms(Y1, [W00, W1], words, result_folder + 'synonyms1.txt')
    synonym_comparison.rank_synonyms(Y2, [W0, W1], words, result_folder + 'synonyms2.txt')

    analogy_tests.test_analogies([Y1, W00, W1], words, 1, result_folder + 'analogy_1_1.txt')
    analogy_tests.test_analogies([Y2, W0, W1], words, 1, result_folder + 'analogy_2_1.txt')
    analogy_tests.test_analogies([Y1, W00, W1], words, 10, result_folder + 'analogy_1_10.txt')
    analogy_tests.test_analogies([Y2, W0, W1], words, 10, result_folder + 'analogy_2_10.txt')
    