#http://www.eki.ee/dict/ekss/index.cgi?callback=jsonp1404206467741&_=1404210268439&Q=konn&Z=json&X=minuparool
import urllib.parse, urllib.request, json, urllib, base64, codecs
import gensim
import re
import sys

import settings

#connection = sqlite3.connect('words.db')
#connection.text_factory = lambda x: unicode(x, "utf-8", "ignore")
#c = connection.cursor()

# Create table
#c.execute('''create table if not exists words (word text, definition text, type text)''')

def exists(n):
	try:
		f = open(n, "r")
		f.close()
		return True
	except IOError:
		return False
		
def validify(n):
	return base64.b64encode(n).replace("+", "_").replace("/", "-").replace("=","")

def getDef(q):
#    print(q)
    url = "http://www.eki.ee/dict/sys/index.cgi"
    url = url + '?' + urllib.parse.urlencode({'callback':'jsonp1404206467741', '_':'1404210268439', 'Q':q, 'Z':'json', 'X':'minuparool'})
#    print(url)
    rsp = urllib.request.urlopen(url)
#	data = rsp.read()
    data = rsp.read().decode('utf-8')
    rsp.close()

    result = json.loads(data[len("jsonp1404206467741("):-len(");")])
    #print(result["resultarr"])
    if len(result["resultarr"]) > 0:
        for item in result["resultarr"]:
            try:
                before_word = 'CLASS="x_m m leitud_id"><span class="leitud_ss">'
                if not (before_word in item):
                    continue
                i = item.index(before_word)
                suffix = item[i + len(before_word):-1]
                j = suffix.index("</span>")
                word = suffix[0:j]
                #print(word)
                if word != q:
                    continue
                suffix = suffix[j:-1]
                i = suffix.index('CLASS="x_syn syn">')
                suffix = suffix[i + len('CLASS="x_syn syn">'):-1]
                j = suffix.index("</span>")
                synonym = suffix[0:j]
                return synonym
            except:
                settings.logger.error("Unexpected error: " + str(sys.exc_info()[0]))
    return ""

def main():
    #model = gensim.models.Word2Vec.load_word2vec_format(settings.get_w2v_model_filename(0), binary = True)
    #settings.logger.info("Generating words...")
    word_regex = "^[a-z\-õäöüšž]+$"
    #word_counts = [(word, model.vocab[word].count) for word in model.vocab.keys() if (word != "-") and (re.search(word_regex, word) != None)]
    #settings.logger.info("Sorting words...")
    #word_counts.sort(key = lambda pair: pair[1], reverse = True)
    #settings.logger.info("Save words...")
    #words = [word[0] for word in word_counts]
    #with open(settings.word2vec_models_path + "cleaned_words.txt", "w") as f:
    #    for word in words:
    #        f.write("%s\n" % word)
    with open(settings.word2vec_models_path + "cleaned_words.txt", "r") as f:
        words = [line.rstrip("\n") for line in f]
    settings.logger.info("Scraping...")
    with open(settings.word2vec_models_path + "synonyms.txt", "w", 1) as f:
        for word in words:
            synonym = getDef(word)
            if re.search(word_regex, synonym) != None:
                f.write(word + " " + synonym + "\n")
            settings.logger.info(word + " " + synonym)
            if word == "":
                break

if __name__ == '__main__':
	main()