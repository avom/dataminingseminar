import codecs
import gensim
import numpy as np

import settings

def calc_mean_distances(Y, WPi):
    distances = [np.sqrt(np.square(Y - W).sum(axis = 1)) for W in WPi]
    sum = distances[0].copy()
    sum = sum * sum
    for d in distances[1:]:
        sum += d * d
    return sum / len(WPi)

if __name__ == "__main__":
    settings.logger.info("Loading Y")
    Y = np.load(settings.Y_filename)

    settings.logger.info("Loading WPi")
    WPi = [np.load(settings.get_WP_filename(i)) for i in range(settings.model_count)]

    settings.logger.info("Loading words")
    words = settings.get_words()

    settings.logger.info("Loading model")
    model_filename = settings.get_w2v_model_filename(0)
    model = gensim.models.Word2Vec.load_word2vec_format(model_filename, binary = True)

    settings.logger.info("Calculating distances")
    mean_distances = calc_mean_distances(Y, WPi)

    settings.logger.info("Generating results")
    with codecs.open(settings.results_path + "word_distances.txt", "w", "utf-8") as outf:
        for i in range(len(words)):
            outf.write(words[i].replace("\"", "''") + "\t" + str(model.vocab[words[i]].index) + "\t" + str(mean_distances[i]) + "\n");

    settings.logger.info("Done")