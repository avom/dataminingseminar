import codecs

# Original file at http://wordnetcode.princeton.edu/standoff-files/core-wordnet.txt
wordnet_filename = 'f:/Projects/DMS/wordnet/wordnet.txt'
pairs_filename = 'f:/Projects/DMS/wordnet/pairs.txt'

if __name__ == '__main__':
    with codecs.open(wordnet_filename, 'r', 'utf-8') as infile:
        with codecs.open(pairs_filename, 'w', 'utf-8') as outfile:
            for line in infile:
                tokens = line.split()
                if len(tokens) <= 3:
                    continue
                
                if len(tokens) == 4:
                    word1 = tokens[2].strip('[]')
                    word2 = tokens[3]
                else:
                    continue
                outfile.write(word1 + ' ' + word2 + '\n')