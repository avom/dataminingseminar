import codecs
import math
import numpy as np
import os.path

from multiprocessing.pool import Pool

import analogy_tests
import iterative_minimization
import procrustes
import settings
import synonym_comparison

#glove_dir = 'f:/Projects/DMS/gigaword/glove/'
glove_dir = 'f:/Projects/DMS/gigaword/glove2/'
#glove_dir = 'f:/Projects/DMS/gigaword/test/'
filename_mask = glove_dir + 'glove.6B.%dd.%s'
#words_filename = glove_dir + 'words.glove.6B.txt'
words_filename = glove_dir + 'words.txt'
synonym_file = 'f:/Projects/DMS/wordnet/pairs.txt'
analogy_questions_file = 'f:/Projects/DMS/analogies/questions-words-shuffled.txt'

def save_words(words):
    settings.logger.info('Saving words...')
    if os.path.exists(words_filename):
        return
    with codecs.open(words_filename, 'w', 'utf-8', buffering = 0) as f:
        for word in words:
            f.write(word + '\n')
        
def load_words(filename = None):
    if filename == None:
        filename = words_filename
    with codecs.open(filename, 'r', 'utf-8') as f:
        lines = f.readlines()
    return [line.strip() for line in lines]

def load_model(size, word_count):
    filename = filename_mask % (size, 'bin.npy')
    if os.path.exists(filename):
        return np.load(filename)

    words = []
    result = np.zeros((word_count, size))
    txtfilename = filename_mask % (size, 'txt')
    with codecs.open(txtfilename, 'r', 'utf-8') as f:
        row = 0
        for line in f:
            tokens = line.split()
            assert len(tokens) == size + 1
            words.append(tokens[0] + '\n')
            for col in range(size):
                result[row, col] = float(tokens[col + 1])
            row += 1
            if row % 10000 == 0:
                settings.logger.info('%d vectors loaded in %s' % (row, txtfilename))
    
    save_words(words)
    np.save(filename, result, False)
    return result

def get_words_from_text_file(filename):
    settings.logger.info('Loading number of words from ' + filename + ' ...')
    words = []
    with codecs.open(filename, 'r', 'utf-8') as f:
        for line in f:
            word = line[0:line.index(' ')]
            words.append(word)
            if len(words) % 10000 == 0:
                settings.logger.info('%d words loaded' % len(words))
    save_words(words)
    return words

def get_dimensionality_from_text_file(filename):
    settings.logger.info('Loading number of dimensions from ' + filename + ' ...')
    with codecs.open(filename, 'r', 'utf-8') as f:
        for line in f:
            return len(line.split()) - 1
    return None

def load_model_from_file(txt_filename, npy_filename):
    if os.path.exists(npy_filename):
        return np.load(npy_filename)

    words = get_words_from_text_file(txt_filename)
    dims = get_dimensionality_from_text_file(txt_filename)

    settings.logger.info('Loading model from ' + txt_filename + ' ...')
    result = np.zeros((len(words), dims))
    with codecs.open(txt_filename, 'r', 'utf-8') as f:
        row = 0
        for line in f:
            tokens = line.split(' ')
            words.append(tokens[0] + '\n')
            for col in range(dims):
                result[row, col] = float(tokens[col + 1])
            row += 1
            if row % 10000 == 0:
                settings.logger.info('%d vectors loaded in %s' % (row, txt_filename))
    
    np.save(npy_filename, result, False)
    return result

def save_model(m, words, filename):
    row_count, col_count = m.shape
    with codecs.open(filename, 'w', 'utf-8') as f:
        for row in range(row_count):
            f.write(words[row])
            for col in range(col_count):
                f.write(' ' + str(m[row, col]))
            f.write('\n')
        
def set_dimensionality(W, d):
    row_count, col_count = W.shape
    result = np.zeros((row_count, d))
    for row in range(row_count):
        for col in range(d):
            result[row, col] = W[row, col] if col < col_count else 0.0
    return result

def rank_synonyms(Y, Wi, words):
    filename = glove_dir + 'synonym_ranks.txt'
    synonyms = settings.get_synonyms(synonym_file)
    synonym_comparison.rank_synonyms(Y, Wi, words, filename, synonyms, limit = None)

def rank_synonyms_thread(thread_index, words, synonyms, result_filename):
    words = load_words()
    Wi = [np.load(glove_dir + 'W%d.npy' % i) for i in range(4)]
    Y = np.load(glove_dir + 'Y.npy')
    synonym_comparison.rank_synonyms(Y, Wi, words, result_filename, synonyms, limit = None)

def rank_synonyms_parallel():
    settings.logger.info('Loading words...')
    words = load_words()

    settings.logger.info('Loading synonyms...')
    synonyms = settings.get_synonyms(synonym_file)
    n = math.ceil(len(synonyms) / settings.thread_count)
    synonyms = [synonyms[i:i + n] for i in range(0, len(synonyms), n)]

    result_filenames = [glove_dir + 'synonym_ranks%d.txt' % i for i in range(settings.thread_count)]

    with Pool(processes = settings.thread_count) as pool:
        for i in range(settings.thread_count):
            pool.apply_async(rank_synonyms_thread, (i, words, synonyms[i], result_filenames[i]))
        pool.close()
        pool.join()

    with codecs.open(glove_dir + 'synonym_ranks.txt', 'w', 'utf-8') as outfile:
        for fname in result_filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)

def test_analogies_thread(thread_index, top_n, questions, filename):
    words = load_words()
    Wi = [np.load(glove_dir + 'W%d.npy' % i) for i in range(4)]
    Y = np.load(glove_dir + 'Y.npy')
    analogy_tests.answer_analogy_questions([Y] + Wi, words, top_n, questions, filename, 75)

def test_analogies_parallel(words, top_n):
    settings.logger.info('Loading questions...')
    questions = settings.get_analogy_questions(analogy_questions_file)
    n = math.ceil(len(questions) / settings.thread_count)
    questions = [questions[i:i + n] for i in range(0, len(questions), n)]

    result_filenames = [glove_dir + 'analogy_results%d.txt' % i for i in range(settings.thread_count)]
    #test_analogies_thread(0, 1, questions[0], result_filenames[0])
    with Pool(processes = settings.thread_count) as pool:
        for i in range(settings.thread_count):
            pool.apply_async(test_analogies_thread, (i, top_n, questions[i], result_filenames[i]))
        pool.close()
        pool.join()

    with codecs.open(glove_dir + 'analogy_results.txt', 'w', 'utf-8') as outfile:
        for fname in result_filenames:
            with codecs.open(fname) as infile:
                for line in infile:
                    outfile.write(line)

def getMeanMatrix(WP0, WP1, Y, d0, d1, dY, m, n):
    for word in dY:
        iy = dY[word]
        count = 0
        if word in d0:
            i0 = d0[word]
            Y[iy, :] = WP0[i0, :]
            count = 1
        if word in d1:
            i1 = d1[word]
            if count == 0:
                Y[iy, :] = WP1[i1, :]
            else:
                Y[iy, :] += WP1[i1, :]
            count += 1
        Y[iy, :] /= count

def calcMeanRMSE(WP0, WP1, Y, d0, d1, dY, m, n):
    WP0_ = Y.copy()
    WP1_ = Y.copy()
    for word in dY:
        iy = dY[word]
        if word in d0:
            i0 = d0[word]
            WP0_[iy, :] = WP0[i0, :]
        if word in d1:
            i1 = d1[word]
            WP1_[iy, :] = WP1[i1, :]
    return iterative_minimization.calcMeanRMSE([WP0_, WP1_], Y)

def calculateP(Y, W0, dY, d0):
    Y0 = np.zeros(W0.shape)
    for word in d0:
        i0 = d0[word]
        iy = dY[word]
        Y0[i0, :] = Y[iy, :]
    return procrustes.calculateP(Y0, W0)

def calculateY(W0, W1, words0, words1, stopThreshold = 0.001, max_it = 0, min_it = 0):
    settings.logger.info("Combining word lists...")
    d0 = {value: index for index, value in enumerate(words0)}
    d1 = {value: index for index, value in enumerate(words1)}
    di = [d0, d1]
   
    wordsY = list(set(words0).union(set(words1)))
    dY = {value: index for index, value in enumerate(wordsY)}
    
    settings.logger.info("Generating initial Y...")
    n = W0.shape[1]
    m = len(wordsY)
    Y = iterative_minimization.generateMatrix(m, n)

    settings.logger.info("Initial Y generated, calculate P-s...")
    old_error = float("inf")
    error = float("inf")
    iteration = 0
    while ((old_error - error > stopThreshold) or (old_error < error) or (iteration == 0)) and ((iteration < max_it) or (max_it == 0)) or (iteration < min_it):
#    while ((old_error - error > stopThreshold) or (old_error < error) or (iteration == 0)) and ((iteration < max_it) or (max_it == 0)) or (iteration < min_it):
        old_error = error
        iteration += 1

        settings.logger.info("Calculating P0...")
        P0 = calculateP(Y, W0, dY, d0)
        settings.logger.info("Calculating P1...")
        P1 = calculateP(Y, W1, dY, d1)

        settings.logger.info("Calculating WP0...")
        WP0 = np.matmul(W0, P0)
        settings.logger.info("Calculating WP1...")
        WP1 = np.matmul(W1, P1)

        settings.logger.info("Calculating Y...")
        getMeanMatrix(WP0, WP1, Y, d0, d1, dY, m, n)
        settings.logger.info("Calculating error...")
        error = calcMeanRMSE(WP0, WP1, Y, d0, d1, dY, m, n)

        #for i in range(len(Wi)):
        #    W = Wi[i]
        #    words = wordsi[i]
        #    P = procrustes.calculateP(Y, W)
        #    Pi.append(P)
        #    WPi.append(np.matmul(W, P))
        #Y = getMeanMatrix(WPi, wordsi, di)
        #error = calcMeanRMSE(WPi, Y)
        settings.logger.info("Error after iteration %d: %f" % (iteration, error))
    return (Y, [P0, P1], [WP0, WP1])

if __name__ == '__main__':
    #settings.logger.info('Loading matrices W0, W1, ...')
    #W0 = load_model_from_file(glove_dir + 'glove.42B.300d.txt', glove_dir + 'W0.npy')
    #W1 = load_model_from_file(glove_dir + 'glove.840B.300d.txt', glove_dir + 'W1.npy')
    #Wi = [W0, W1]

    settings.logger.info('Loading words...')
    #words = load_words()
    words0 = load_words(glove_dir + "words0.txt")
    words1 = load_words(glove_dir + "words1.txt")
    wordsY = list(set(words0).union(set(words1)))

    #Y, Pi, WPi = calculateY(W0, W1, words0, words1)

    #word_count = 400 * 1000
    #Wi = [load_model(d, word_count) for d in [50, 100, 200, 300]]
    #words = load_words()
    #settings.logger.info('Setting dimensionalities...')
    #Wi = [set_dimensionality(W, 300) for W in Wi]
    #for i in range(len(Wi)):
    #    np.save(glove_dir + 'W%d' % i, Wi[i], False)

    #Wi = [np.load(glove_dir + 'W%d.npy' % i) for i in range(4)]
    #(Y, Pi, WPi) = iterative_minimization.calculateY(Wi, calculateP = procrustes.calculateP, max_it = 0)
    #settings.logger.info('Saving Y...')
    #np.save(glove_dir + 'Y', Y, False)
    #settings.logger.info('Saving matrices P0, P1, ...')
    #for i in range(len(Pi)):
    #    np.save(glove_dir + 'P%d' % i, Pi[i], False)
    #settings.logger.info('Saving matrices WP0, WP1, ...')
    #for i in range(len(WPi)):
    #    np.save(glove_dir + 'WP%d' % i, WPi[i], False)

    #Y = np.load(glove_dir + 'Y.npy')
    #save_model(Y, words, glove_dir + 'Y.txt')
    
    #rank_synonyms_parallel()
    #rank_synonyms(Y, Wi, words)
    

    #test_analogies_parallel(words, 1)
    #test_analogies_parallel(words, 10)

    ############# Test synonyms  #########################

    settings.logger.info('Combining words...')
    words0Set = set(words0)
    words1Set = set(words1)
    wordsSet = words0Set.intersection(words1Set)
    words = list(wordsSet)

    d0 = {value: index for index, value in enumerate(words0)}
    d1 = {value: index for index, value in enumerate(words1)}
    dY = {value: index for index, value in enumerate(wordsY)}

    settings.logger.info('Loading W0...')
    W = np.load(glove_dir + 'W0.npy')
    m = len(words)
    n = W.shape[1]
    W0 = np.zeros((m, n))
    for i in range(len(words)):
        j = d0[words[i]]
        W0[i, :] = W[j, :]

    settings.logger.info('Loading W1...')
    W = np.load(glove_dir + 'W1.npy')
    W1 = np.zeros((m, n))
    for i in range(len(words)):
        j = d1[words[i]]
        W1[i, :] = W[j, :]

    settings.logger.info('Loading Y...')
    W = np.load(glove_dir + 'Y.npy')
    Y = np.zeros((m, n))
    for i in range(len(words)):
        j = dY[words[i]]
        Y[i, :] = W[j, :]
    W = None
    rank_synonyms(Y, [W0, W1], words)

    ############# Test analogies #########################
    #words = dict()
    #words['Y'] = wordsY
    #words['W0'] = words0
    #words['W1'] = words1
    #questions = settings.get_analogy_questions(analogy_questions_file)
    #for top_n in [1, 10]:
    #    for matrix_name in ['Y', 'W0', 'W1']:
    #        if (top_n == 1) and (matrix_name == 'Y'):
    #            continue
    #        settings.logger.info('Running analogy test for %s with recall at %d...' % (matrix_name, top_n))
    #        A = np.load(glove_dir + matrix_name + '.npy')
    #        filename = glove_dir + 'analogy_' + str(top_n) + '_' + matrix_name + '.txt';
    #        analogy_tests.answer_analogy_questions([A], words[matrix_name], top_n, questions, filename, 300)

    settings.logger.info('Done')

