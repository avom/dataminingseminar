import codecs
import gensim
import numpy as np

import settings
import synonym_comparison

def check_analogy(A, i0, i1, i2, i3):
    cos0 = synonym_comparison.cosine_similarities(A, A[i0])
    cos1 = synonym_comparison.cosine_similarities(A, A[i1])
    cos2 = synonym_comparison.cosine_similarities(A, A[i2])
    sum_cos = cos1 - cos0 + cos2
    top_indices = sum_cos.argsort()[-settings.analogy_test_top_n - 3::]
    top_indices = top_indices[(top_indices != i0) & (top_indices != i1) & (top_indices != i2)]
    top_indices = top_indices[-settings.analogy_test_top_n]
    return i3 in top_indices

def check_analogy_normalized(A, i0, i1, i2, i3):
    cos0 = synonym_comparison.cosine_similarities_normalized(A, A[i0])
    cos1 = synonym_comparison.cosine_similarities_normalized(A, A[i1])
    cos2 = synonym_comparison.cosine_similarities_normalized(A, A[i2])
    sum_cos = cos1 - cos0 + cos2
    top_indices = sum_cos.argsort()[-settings.analogy_test_top_n - 3::]
    top_indices = top_indices[(top_indices != i0) & (top_indices != i1) & (top_indices != i2)]
    top_indices = top_indices[-settings.analogy_test_top_n:]
    return i3 in top_indices

def check_analogy_benchmark(A, i0, i1, i2, i3):
    settings.logger.info('BEFORE')
    cos0 = synonym_comparison.cosine_similarities(A, A[i0])
    settings.logger.info('1')
    cos1 = synonym_comparison.cosine_similarities(A, A[i1])
    settings.logger.info('2')
    cos2 = synonym_comparison.cosine_similarities(A, A[i2])
    settings.logger.info('3')
    sum_cos = cos1 - cos0 + cos2
    settings.logger.info('4')
    top_indices = sum_cos.argsort()[-settings.analogy_test_top_n - 3::]
    settings.logger.info('5')
    top_indices = top_indices[(top_indices != i0) & (top_indices != i1) & (top_indices != i2)]
    settings.logger.info('6')
    top_indices = top_indices[0:settings.analogy_test_top_n]
    settings.logger.info('AFTER')
    return i3 in top_indices

def check_analogy_mul(A, i0, i1, i2, i3):
    cos0 = synonym_comparison.cosine_similarities(A, A[i0])
    cos1 = synonym_comparison.cosine_similarities(A, A[i1])
    cos2 = synonym_comparison.cosine_similarities(A, A[i2])
    sum_cos = cos1 * cos2 / (cos0 + 0.001)
    top_indices = sum_cos.argsort()[-settings.analogy_test_top_n - 3::]
    top_indices = top_indices[(top_indices != i0) & (top_indices != i1) & (top_indices != i2)]
    top_indices = top_indices[0:settings.analogy_test_top_n]
    return i3 in top_indices

def normalize(matrix):
    result = matrix.copy()
    magnitudes = np.linalg.norm(matrix, axis = 1)
    return result / magnitudes[:, None]

def test_analogies(matrices, words, top_n, results_file):
    settings.analogy_test_top_n = top_n
    norm_matrices = [normalize(matrix) for matrix in matrices]
    matches = [0 for A in matrices]
    wordsSet = set(words)
    test_count = 0
    with codecs.open(settings.data_path + "analogy\\analogy.txt", "r", "utf-8", buffering = 0) as inf:
        with codecs.open(results_file, "w", "utf-8", buffering = 0) as outf:
            for line in inf:
                test = line.rstrip("\r\n").split(" ")
                if not (test[0] in wordsSet) or not (test[1] in wordsSet) or not (test[2] in wordsSet) or not (test[3] in wordsSet):
                    continue

                test_count += 1
                i0 = words.index(test[0].lower())
                i1 = words.index(test[1].lower())
                i2 = words.index(test[2].lower())
                i3 = words.index(test[3].lower())

                settings.logger.info(line.rstrip("\r\n"))
                outf.write(line.rstrip("\r\n)"))
                for i in range(len(matrices)):
                    is_analogy = check_analogy(matrices[i], i0, i1, i2, i3)
                    outf.write("\t%s" % is_analogy)
                outf.write("\n")

def answer_analogy_questions(matrices, words, top_n, questions, results_file, limit = None):
    settings.analogy_test_top_n = top_n
    norm_matrices = [normalize(matrix) for matrix in matrices]
    matches = [0 for A in norm_matrices]
    wordsSet = set(words)
    wordIndices = {value: index for index, value in enumerate(words)}
    test_count = 0
    with codecs.open(results_file, "w", "utf-8", buffering = 0) as outf:
        for line in questions:
            test = line.rstrip("\r\n").split(" ")
            if not (test[0] in wordsSet) or not (test[1] in wordsSet) or not (test[2] in wordsSet) or not (test[3] in wordsSet):
                continue

            test_count += 1
            i0 = wordIndices[test[0]]
            i1 = wordIndices[test[1]]
            i2 = wordIndices[test[2]]
            i3 = wordIndices[test[3]]

            settings.logger.info(line.rstrip("\r\n"))
            outf.write(line.rstrip("\r\n)"))
            for i in range(len(norm_matrices)):
                is_analogy = check_analogy_normalized(norm_matrices[i], i0, i1, i2, i3)
                outf.write("\t" + str(is_analogy).upper())
            outf.write("\n")
            if test_count == limit:
                break

if __name__ == "__main__":
    settings.logger.info("Loading Y")
    Y = np.load(settings.Y_filename)

    settings.logger.info("Loading Wi")
    Wi = [np.load(settings.get_w2v_matrix_filename(i)) for i in range(settings.model_count)]

    settings.logger.info("Loading words")
    words = settings.get_words()

    #filename = settings.get_w2v_model_filename(0)
    #model = gensim.models.Word2Vec.load_word2vec_format(filename, binary = True)
    success = 0

    #print(normalize(np.matrix([[1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6]])))
    test_count = 0
    matrices = [normalize(matrix) for matrix in [Y] + Wi]
    #matrices = [Wi[0]]
    matches = [0 for A in matrices]
    with codecs.open(settings.data_path + "analogy\\analogy.txt", "r", "utf-8") as inf:
        #with codecs.open(settings.results_path + "analogy_results_mul" + str(settings.analogy_test_top_n) + ".txt", "w", "utf-8") as outf:
        with codecs.open(settings.results_path + "analogy_results2_" + str(settings.analogy_test_top_n) + ".txt", "w", "utf-8") as outf:
            for line in inf:
                test = line.rstrip("\r\n").split(" ")
                if not (test[0] in words) or not (test[1] in words) or not (test[2] in words) or not (test[3] in words):
                    continue
        
                #settings.logger.info(line.rstrip("\r\n"))
                #result = model.most_similar([test[1], test[2]], [test[0]])
                #if result[0][0] == test[3]:
                #    success += 1
                #continue

                test_count += 1
                i0 = words.index(test[0])
                i1 = words.index(test[1])
                i2 = words.index(test[2])
                i3 = words.index(test[3])

                settings.logger.info(line.rstrip("\r\n"))
                outf.write(line.rstrip("\r\n)"))
                for i in range(len(matrices)):
                    is_analogy = check_analogy(matrices[i], i0, i1, i2, i3)
                    #is_analogy = check_analogy_mul(matrices[i], i0, i1, i2, i3)
                    outf.write("\t%s" % is_analogy)
                    if is_analogy:
                        matches[i] += 1
                outf.write("\n")
                
    print("Successes", success)
    print("Tests", test_count, "Matches", matches)