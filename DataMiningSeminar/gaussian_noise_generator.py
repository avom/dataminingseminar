from multiprocessing.pool import Pool

import numpy as np

import settings

def generate_noise(a):
    col_std = np.std(a, axis = 0)
    for i in range(a.shape[1]):
        a[::, i] += np.random.normal(0, col_std[i])

def generate_noisy_matrix(index):
    infile = settings.get_w2v_matrix_filename(0)
    matrix = np.load(infile)
    generate_noise(matrix)
    outfile = settings.get_noisy_w2v_matrix_filename(index)
    np.save(outfile, matrix)

if __name__ == '__main__':
    #a = np.array([[1.0, 2], [1, 3], [2, 3]])
    #generate_noise(a)
    #print(a)
    #a = np.array([[1.0, 2], [1, 3], [2, 3]])
    #generate_noise(a)
    #print(a)
    with Pool(processes = settings.thread_count) as pool:
        for i in range(settings.model_count):
            pool.apply_async(generate_noisy_matrix, (i,))
        pool.close()
        pool.join()