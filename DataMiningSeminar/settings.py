import codecs
import logging
import os.path
import pickle
import sys

program = os.path.basename(sys.argv[0])
logger = logging.getLogger(program)
 
logging.basicConfig(format='%(asctime)s: %(levelname)s: %(message)s', stream = sys.stdout)
logging.root.setLevel(level=logging.INFO)

data_path = 'f:\\Projects\\DMS\\data\\alligaator\\'
log_file_name = data_path + '..\\log.txt'
#log_file_name = 'f:/Projects/DMS/gigaword/glove/log.txt'

logger_formatter = logging.Formatter('%(asctime)s: %(levelname)s: %(message)s')
logger_filehandler = logging.FileHandler(log_file_name)
logger_filehandler.setFormatter(logger_formatter)
logger.addHandler(logger_filehandler)
logger.info("running %s" % ' '.join(sys.argv))

dimensions = 200
model_count = 10
thread_count = 4

analogy_test_top_n = 1

optimization_method = "procrustes"
#optimization_method = "linear"
nodalida_data_path = "..\\..\\data\\nodalida\\"

if len(sys.argv) > 3:
    data_path = sys.argv[3]    
if len(sys.argv) > 2:
    dimensions = int(sys.argv[2])
if len(sys.argv) > 1:
    optimization_method = sys.argv[1]

logger.info("optimization_method = \"%s\", dimensions = %d, data_path = \"%s\"" % (optimization_method, dimensions, data_path))

word2vec_models_path = data_path + "w2v\\"
results_path = data_path + optimization_method + str(dimensions) + "\\"

words_filename = word2vec_models_path + "words.txt"
Y_filename = results_path + "Y.%d.npy" % dimensions
mean_variances_filename = results_path + "mean_vars_%d.txt" % dimensions

def get_w2v_model_filename(index):
    return word2vec_models_path + "%d.w10.s%d.bin" % (index, dimensions)

def get_w2v_matrix_filename(index):
    return word2vec_models_path + "w10.s%d.%d.bin.npy" % (dimensions, index)

def get_noisy_w2v_matrix_filename(index):
    return word2vec_models_path + "w10.s%d.%d.bin.noisy.npy" % (dimensions, index)

def get_P_filename(index):
    return results_path + "P.s%d.%d.npy" % (dimensions, index)

def get_WP_filename(index):
    return results_path + "WP.s%d.%d.npy" % (dimensions, index)

def get_diff_of_WP_Y_filename(index):
    return results_path + "diff_WP_Y.s%d.%d.npy" % (dimensions, index)

def get_vars_of_diff_WP_Y_filename(index):
    return results_path + "vars_of_diff_WP_Y.s%d.%d.npy" % (dimensions, index)

def get_words():
    with open(words_filename, "rb") as f:
        return pickle.load(f)

def get_synonyms(filename = None):
    if filename is None:
        filename = data_path + "..\\synonyms\\synonyms.txt"
    with open(filename, "r") as f:
        return [line.rstrip("\n").split(" ") for line in f]

def get_analogy_questions(filename):
    with codecs.open(filename, "r", 'utf-8') as f:
        return f.readlines()
