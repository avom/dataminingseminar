from numpy import linalg

import logging
import numpy as np
import os
import random
import sys

import procrustes
import settings
import WordEmbeddings

def generateMatrix(m, n):
    return np.random.rand(m, n) * 2 - 1

def calculatePExact(Y, W):
    WleftInv = linalg.pinv(W)
    return np.matmul(WleftInv, Y)

def getMeanMatrix(Wi):
    m, n = Wi[0].shape
    result = np.zeros((m, n))
    for W in Wi:
        result = result + W
    result /= len(Wi)
    return result

def calcMeanRMSE(WPi, Y):
    result = 0
    for WP in WPi:
        (m, n) = WP.shape
        result += np.linalg.norm(WP - Y) / np.sqrt(m * n)
    return result / len(WPi)

def scaleMatrix(mat):
    for col in range(0, mat.shape[1]):
        mat[:, col] = mat[:, col] / np.std(mat[:, col])

def getMeanColVar(mat):
    return np.mean([np.var(col) for col in mat.T])

def calculateY(Wi, stopThreshold = 0.001, calculateP = calculatePExact, Y0 = None, use_scaling = False, max_it = 0, min_it = 0):
    settings.logger.info("Generating initial Y...")
    m, n = Wi[0].shape
    if Y0 is None:
        Y0 = generateMatrix(m, n)
    Y = np.copy(Y0)
    settings.logger.info("Initial Y generated, calculate P-s...")
    old_error = float("inf")
    error = float("inf")
    iteration = 0
    while ((old_error - error > stopThreshold) or (old_error < error) or (iteration == 0)) and ((iteration < max_it) or (max_it == 0)) or (iteration < min_it):
#    while ((old_error - error > stopThreshold) or (old_error < error) or (iteration == 0)) and ((iteration < max_it) or (max_it == 0)) or (iteration < min_it):
        old_error = error
        if use_scaling:
            scaleMatrix(Y)
        iteration += 1
        Pi = []
        WPi = []
        for W in Wi:
            P = calculateP(Y, W)
            Pi.append(P)
            WPi.append(np.matmul(W, P))
        Y = getMeanMatrix(WPi)
        error = calcMeanRMSE(WPi, Y)
        settings.logger.info("Error after iteration %d: %f; mean column variance: %f; ||Y - Y0|| = %f" % (iteration, error, getMeanColVar(Y), calcMeanRMSE([Y], Y0)))
    return (Y, Pi, WPi)

def combine_embeddings_on_disk(embedding_files, dims, stopThreshold = 0.001):
    #for embedding in embeddings:
    #    if embedding.dims() < dims:
    #        settings.logger.info('Extending %s dimensions from %d to %d' % (embedding.name, embedding.dims(), dims))
    #        embedding.extend(dims)

    settings.logger.info('Combining word lists...')
    wordsY_set = set()
    for filename in embedding_files:
        settings.logger.info('... from %s' % filename)
        embedding = WordEmbeddings.WordEmbeddings(filename)
        wordsY_set = wordsY_set.union(set(embedding.words))
    wordsY = list(wordsY_set)
    indexY = {value: index for index, value in enumerate(wordsY)}
    settings.logger.info('Found %d distinct words' % len(wordsY))

    settings.logger.info('Generating initial Y...')
    n = dims
    m = len(wordsY)
    Y = generateMatrix(m, n)
    return Y

def calculateP_between_embeddings(Y, W):
    Y0 = np.zeros(W.W.shape)
    for word in W.index:
        i0 = W.index[word]
        iy = Y.index[word]
        Y0[i0, :] = Y.W[iy, :]
    return procrustes.calculateP(Y0, W.W)

def combine_embeddings(embeddings, dims, stopThreshold = 0.001, max_it = 1000000):
    for embedding in embeddings:
        if embedding.dims() < dims:
            settings.logger.info('Extending %s dimensions from %d to %d' % (embedding.name, embedding.dims(), dims))
            embedding.extend(dims)

    settings.logger.info('Combining word lists...')
    Y = WordEmbeddings.WordEmbeddings(filename = '', name = 'Y')
    wordsY_set = set()
    for W in embeddings:
        wordsY_set = wordsY_set.union(set(W.words))
    Y.words = list(wordsY_set)
    Y.index = {value: index for index, value in enumerate(Y.words)}
    settings.logger.info('Found %d distinct words' % len(Y.words))

    settings.logger.info('Generating initial Y...')
    n = dims
    m = len(Y.words)
    Y.W = generateMatrix(m, n)

    settings.logger.info('Initial Y generated, calculate P-s...')
    old_error = float('inf')
    error = float('inf')
    iteration = 0

    while ((old_error - error > stopThreshold) or (iteration == 0)) and (iteration < max_it):
        old_error = error
        iteration += 1
        settings.logger.info('Iteration %d ' % iteration)
        settings.logger.info('  Scaling Y...')
        scaleMatrix(Y.W)

        settings.logger.info('  Calculating P-s...')
        for W in embeddings:
            settings.logger.info('    Calculating P for %s...' % W.name)
            W.P = calculateP_between_embeddings(Y, W)

        settings.logger.info('  Calculating WP-s...')
        newY = np.zeros_like(Y.W)
        counts = [0 for i in range(len(Y.words))]
        for W in embeddings:
            settings.logger.info('    Processing %s...' % W.name)
            WP = W.WP()
            for word in W.words:
                wi = W.index[word]
                yi = Y.index[word]
                counts[yi] += 1
                newY[yi, :] += W.W[wi, :]
        settings.logger.info('    Adjusting Y based on counts...')
        for i in range(len(counts)):
            newY[i, :] /= counts[i]        

        settings.logger.info('  Calculating error...')
        error = 0
        for W in embeddings:
            settings.logger.info('    Processing %s...' % W.name)
            WP = W.WP()
            err = 0.0
            for word in W.words:
                wi = W.index[word]
                yi = Y.index[word]
                d = newY[yi, :] - WP[wi, :]
                err += sum(d * d)
            err /= np.sqrt(WP.shape[0] * WP.shape[1])
            error += err
        error /= len(embeddings)        
        settings.logger.info('  Error: ' + str(error))
        Y.W = newY

    return Y