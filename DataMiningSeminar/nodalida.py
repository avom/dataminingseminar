from gensim.models.word2vec import Word2Vec, LineSentence

import numpy as np
import os.path
import pickle

import analogy_tests
import iterative_minimization
import preparation
import procrustes
import settings
import synonym_comparison

def produce_combined_model():
    sentences = LineSentence(settings.nodalida_data_path + 'combined.txt')
    model = Word2Vec(sentences, sg = 1, size = 300, window = 10, min_count = 5, workers = 8)
    model.save(settings.nodalida_data_path + 'combined.w2v')
    preparation.save_numpy_array(settings.nodalida_data_path + 'combined.npy', model, words)

def load_words():
    filepath = settings.nodalida_data_path + 'words.txt'
    if not os.path.exists(filepath):
        return None
    with open(filepath, 'rb') as f:
        return pickle.load(f)

def load_matrices():
    result = []
    for filename in ['est_intersection.npy', 'etwiki_intersection.npy']:
        filepath = settings.nodalida_data_path + filename
        if os.path.exists(filepath):
            result.append(np.load(filepath))
    return result

def save_minimization_results(Y, Pi, WPi, method):
    np.save(settings.nodalida_data_path + 'Y_' + method + '.npy', Y, False)

    for i in range(len(Wi)):
        P_filename = settings.nodalida_data_path + 'P' + str(i) + '_' + method + '.npy'
        np.save(P_filename, Pi[i], False)
        WP_filename = settings.nodalida_data_path + 'WP' + str(i) + '_' + method + '.npy'
        np.save(WP_filename, WPi[i], False)

def load_Y(method):
    filename = settings.nodalida_data_path + 'Y_' + method + '.npy'
    if os.path.exists(filename):
        return np.load(filename)
    return None

def rank_synonyms_if_missing(Y, Wi, words, method):
    filename = settings.nodalida_data_path + 'synonym_ranks_' + method +'.txt'
    if not os.path.exists(filename):
        settings.logger.info('Rank synonyms in Y_' + method)
        synonym_comparison.rank_synonyms(Y, Wi, words, filename)

def test_analogies_if_missing(matrices, words, top_n):
    filename = settings.nodalida_data_path + 'analogy_test_results_top_' + str(top_n) + '.txt'
    if not os.path.exists(filename):
        analogy_tests.test_analogies(matrices, words, top_n, filename)

if __name__ == '__main__':
    words = load_words()
    
    Y = np.load(settings.nodalida_data_path + 'combined.npy')
    rank_synonyms_if_missing(Y, [], words, 'combined')
    test_analogies_if_missing([Y], words, 1)
    test_analogies_if_missing([Y], words, 10)
    test_analogies_if_missing([Y], words, 100)

    Wi = load_matrices()
    if (words is None) or (len(Wi) <= 1):
        est = Word2Vec.load(settings.nodalida_data_path + 'koond.w2v')
        etwiki = Word2Vec.load(settings.nodalida_data_path + 'etwiki.w2v')
   
        est_words = list(est.vocab.keys())
        etwiki_words = list(etwiki.vocab.keys())

        settings.logger.info('Estonian reference corpus: %d words, %d dimensions' % (len(est_words), est[est_words[0]].shape[0]))
        settings.logger.info('Estonian Wikipedia corpus: %d words, %d dimensions' % (len(etwiki_words), etwiki[etwiki_words[0]].shape[0]))

        words = list(set(est_words).intersection(etwiki_words))
        settings.logger.info('Common words: %d' % len(words))

        preparation.save_words(settings.nodalida_data_path + 'words.txt', words)
        preparation.save_numpy_array(settings.nodalida_data_path + 'est_intersection.npy', est, words)
        preparation.save_numpy_array(settings.nodalida_data_path + 'etwiki_intersection.npy', etwiki, words)
        Wi = load_matrices()

    if not os.path.exists(settings.nodalida_data_path + 'combined.npy'):
        produce_combined_model()

    Y_slrce = load_Y("slrce")
    if Y_slrce is None:
        settings.logger.info('Optimizing via SLRCE')
        (Y_slrce, Pi_slrce, WPi_slrce) = iterative_minimization.calculateY(Wi, use_scaling = True)
        save_minimization_results(Y_slrce, Pi_slrce, WPi_slrce, 'slrce')

    Y_sopp = load_Y("sopp")
    if Y_sopp is None:
        settings.logger.info('Optimizing via SOPP')
        (Y_sopp, Pi_sopp, WPi_sopp) = iterative_minimization.calculateY(Wi, calculateP = procrustes.calculateP, max_it = 0)
        save_minimization_results(Y_sopp, Pi_sopp, WPi_sopp, 'sopp')
    
    rank_synonyms_if_missing(Y_slrce, Wi, words, 'slrce')
    rank_synonyms_if_missing(Y_sopp, Wi, words, 'sopp')

    test_analogies_if_missing([Y_slrce, Y_sopp] + Wi, words, 1)
    test_analogies_if_missing([Y_slrce, Y_sopp] + Wi, words, 10)
    test_analogies_if_missing([Y_slrce, Y_sopp] + Wi, words, 100)
