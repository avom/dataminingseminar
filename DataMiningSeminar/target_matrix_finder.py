import iterative_minimization
import procrustes

import numpy as np
import settings

import sys

if __name__ == "__main__":
    Wi = [np.load(settings.get_w2v_matrix_filename(i)) for i in range(settings.model_count)]

    if settings.optimization_method == "linear":
        (Y, Pi, WPi) = iterative_minimization.calculateY(Wi, use_scaling = True)
    elif settings.optimization_method == "procrustes":
        (Y, Pi, WPi) = iterative_minimization.calculateY(Wi, calculateP = procrustes.calculateP, max_it = 0)
    else:
        settings.logger.info("Unknown optimization method: " + settings.optimization_method)
        sys.exit(0)

    np.save(settings.Y_filename, Y, False)

    for i in range(len(Wi)):
        np.save(settings.get_P_filename(i), Pi[i], False)
        np.save(settings.get_WP_filename(i), WPi[i], False)

