from gensim.models.word2vec import Word2Vec, LineSentence

import numpy as np
import os.path
import pickle

import analogy_tests
import iterative_minimization
import preparation
import procrustes
import settings
import synonym_comparison

def rank_synonyms(Y, Wi, words, method):
    filename = settings.results_path + 'synonym_ranks_noisy_' + method +'.txt'
    synonym_comparison.rank_synonyms(Y, Wi, words, filename)

def test_analogies(matrices, words, top_n):
    filename = settings.results_path + 'analogy_test_results_noisy_top_' + str(top_n) + '.txt'
    analogy_tests.test_analogies(matrices, words, top_n, filename)

def save_minimization_results(Y, Pi, WPi, method):
    np.save(settings.nodalida_data_path + 'Y_' + method + '_noisy.npy', Y, False)

    for i in range(len(Wi)):
        P_filename = settings.results_path + 'P' + str(i) + '_' + method + '_noisy.npy'
        np.save(P_filename, Pi[i], False)
        WP_filename = settings.results_path + 'WP' + str(i) + '_' + method + '_noisy.npy'
        np.save(WP_filename, WPi[i], False)

if __name__ == '__main__':
    settings.logger.info('Loading words...')
    words = settings.get_words()
    
    settings.logger.info('Loading input matrices...')
    Wi = [np.load(settings.get_noisy_w2v_matrix_filename(i)) for i in range(settings.model_count)]

    if settings.optimization_method == "procrustes":
        (Y_sopp, Pi_sopp, WPi_sopp) = iterative_minimization.calculateY(Wi, calculateP = procrustes.calculateP, max_it = 0)
    else:
        (Y_sopp, Pi_sopp, WPi_sopp) = iterative_minimization.calculateY(Wi, max_it = 0)
    save_minimization_results(Y_sopp, Pi_sopp, WPi_sopp, 'sopp')

    rank_synonyms(Y_sopp, Wi, words, 'sopp')

    test_analogies([Y_sopp] + Wi, words, 1)
    test_analogies([Y_sopp] + Wi, words, 10)