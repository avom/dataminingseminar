from bs4 import BeautifulSoup
from gensim.models.word2vec import LineSentence, Word2Vec

import codecs
import estnltk
import os
import pickle

import settings

def get_sentences(soup):
    for p in soup.find_all('p'):
        if p.find('author') != None:
            continue;
        for s in p.find_all('s'):
            tokens = s.getText().split()
            sentence = [token if token.replace('-', '').isalpha() else 'num' for token in tokens if token.replace('-', '').isalnum()]
            yield sentence

def get_texts(soup, div_index = 1):
    div_name = 'div' + str(div_index)
    divs = soup.find_all(div_name)
    if (len(divs) == 0):
        return list(get_sentences(soup))
    result = []
    for div in divs:
        result += get_texts(div, div_index + 1)
    return result

def get_file_texts(filename):
    soup = BeautifulSoup(codecs.open(filename, 'r', 'utf-8'), 'lxml')
    return get_texts(soup)

def count_files(dir):
    result = 0
    for subdir, dirs, files in os.walk(dir):
        for file in files:
            if file.endswith('.xml'):
                result += 1
    return result
    
if __name__ == '__main__':

    dir = settings.nodalida_data_path + 'koond\\'
    file_count = count_files(dir)
    completed_count = 0
    texts = []
    for subdir, dirs, files in os.walk(dir):
        for file in files:
            if file.endswith('.xml'):
                filename = os.path.join(subdir, file)
                settings.logger.info('Processing ' + filename)
                texts += get_file_texts(filename)
                completed_count += 1
                settings.logger.info('%d files out of %d completed' % (completed_count, file_count))

    settings.logger.info("Dump sentences to pickle...")
    with open(settings.nodalida_data_path + 'koond.pickle', 'wb') as f:
        pickle.dump(texts, f)
    with open(settings.nodalida_data_path + 'koond.pickle', 'rb') as f:
        texts = pickle.load(f)
    #texts = [[w.decode('utf-8') for w in l] for l in texts]
    settings.logger.info("Dump sentences to text file...")
    with codecs.open(settings.nodalida_data_path + 'koond.txt', 'w', 'utf-8') as f:
        for sentence in texts:
            f.write(' '.join(sentence) + '\n')

    sentences = texts
    #words = dict()
    #settings.logger.info('count words...')
    #for sentence in sentences:
    #    for word in sentence:
    #        if not (word in words):
    #            words[word] = 1
    #            print(word)
    #        else:
    #            words[word] += 1
    #sentences = LineSentence(settings.nodalida_data_path + 'koond.txt')
    model = Word2Vec(sentences, sg = 1, size = 300, window = 10, min_count = 5, workers = 8)
    model.save(settings.nodalida_data_path + 'koond.w2v')

    model = Word2Vec.load(settings.nodalida_data_path + 'koond.w2v')

    print('test')