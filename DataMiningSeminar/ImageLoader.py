from PIL import Image

import math
import numpy as np
import random

import DataMiningSeminar
import procrustes

matrices_to_use = 10
image_name = "aerial.png"

def get_rotated_matrix(A, angle):
    result = np.zeros(A.shape)
    for i in range(A.shape[0]):
        (x, y) = A[i]
        result[i, 0] = x * math.cos(angle) - y * math.sin(angle)
        result[i, 1] = x * math.sin(angle) + y * math.cos(angle)
    return result

def save_matrix(original_image, original_pixels, matrix, filename):
    image = Image.new(original_image.mode, original_image.size)
    for i in range(cols * rows):
        row = matrix[i]
        old_col = int(round(cols * (row[0] + 1) / 2))
        old_row = int(round(rows * (row[1] + 1) / 2))
        if old_col < 0 or old_col >= cols or old_row < 0 or old_row >= rows:
            continue
        new_col = i // rows
        new_row = i % rows
        image.putpixel((new_col, new_row), original_pixels[old_col, old_row])
    image.save(filename)
    print("Saved", filename)


print("Loading original image...")
image = Image.open(image_name)
pixels = image.load()

print("Initializing the original matrix W0...")
W0 = np.zeros((image.size[0] * image.size[1], 2))
(cols, rows) = image.size
for c in range(cols):
    for r in range(rows):
        row = c * rows + r
        W0[row] = [2.0 * c / cols - 1.0, 2.0 * r / rows - 1.0];

print("Generating randomly rotated matrices...")
Wi = [get_rotated_matrix(W0, 2.0 * math.pi * random.random()) if i > 0 else W0 for i in range(matrices_to_use + 1)]

print("Saving rotated images...")
for i in range(len(Wi)):
    save_matrix(image, pixels, Wi[i], "W%d.png" % i)

#(Y, Pi) = DataMiningSeminar.calculateY(Wi[1:], 0.01, DataMiningSeminar.calculatePExact, Wi[0])
(Y, Pi) = DataMiningSeminar.calculateY(Wi[1:], 0.01, procrustes.calculateP)

save_matrix(image, pixels, Y, "Y.png")