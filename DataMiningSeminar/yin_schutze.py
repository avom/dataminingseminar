import codecs
import numpy as np

import analogy_tests
import iterative_minimization
import settings
import WordEmbeddings

work_dir = 'c:\\Projects\\DataMiningSeminar\\data\\yin_scutze_test\\'
work_dir = 'c:\\Projects\\DataMiningSeminar\\data\\yin_schutze\\'
work_dir = 'f:\\Projects\\DMS\\data\\yin_schutze\\'
#work_dir = 'f:\\Projects\\DMS\\data\\yin_schutze_test\\'

if __name__ == "__main__":
    settings.logger.info('Loading models...')
    #hlbl = WordEmbeddings.WordEmbeddings(work_dir + 'hlbl_100', 'HLBL')
    #huang = WordEmbeddings.WordEmbeddings(work_dir + 'huang_50', 'Huang')
    #glove = WordEmbeddings.WordEmbeddings(work_dir + 'glove_300', 'Glove')
    #cw = WordEmbeddings.WordEmbeddings(work_dir + 'cw_200', 'CW')
    #word2vec = WordEmbeddings.WordEmbeddings(work_dir + 'word2vec_300', 'Word2Vec', is_w2v = True)

    hlbl = WordEmbeddings.WordEmbeddings(work_dir + 'hlbl_100', 'HLBL')
    huang = WordEmbeddings.WordEmbeddings(work_dir + 'huang_50', 'Huang')
    glove = WordEmbeddings.WordEmbeddings(work_dir + 'glove_300', 'Glove')
    cw = WordEmbeddings.WordEmbeddings(work_dir + 'cw_200', 'CW')
    word2vec = WordEmbeddings.WordEmbeddings(work_dir + 'word2vec_300', 'Word2Vec', is_w2v = True)

    glove.vertical_normalize()
    embeddings = [hlbl, huang, glove, cw, word2vec]
    embeddings = [glove, word2vec]
    for emb in embeddings:
        emb.normalize()
    #glove.W = glove.W * 8
    #word2vec.W = word2vec.W * 8
    
    settings.logger.info('Training Y...')
    files = [work_dir + filename for filename in ['hlbl_100', 'huang_50', 'glove_300', 'cw_200', 'word2vec_300']]
    files = [work_dir + filename for filename in ['glove_300', 'word2vec_300']]
    Y = iterative_minimization.combine_embeddings(embeddings, dims = 300)

    settings.logger.info('Saving results...')
    target_model_name = 'X'
    np.save(work_dir + target_model_name + '.npy', Y.W, allow_pickle = False)
    with codecs.open(work_dir + target_model_name + '_words.txt', 'w', 'utf-8', buffering = 0) as outf:
        for word in Y.words:
            outf.write(word + '\n')
        
    #for embedding in embeddings:
    #    np.save(work_dir + 'P_' + embedding.name + '.npy', embedding.P, allow_pickle = False)

    #hlbl = None
    #huang = None
    #glove = None
    #cw = None
    #word2vec = None
    #Y = np.load(work_dir + 'Y.npy', allow_pickle = False)
    #questions = settings.get_analogy_questions('f:\\Projects\\DMS\\analogies\\questions-words-sem.txt')
  
    #analogy_tests.answer_analogy_questions([Y], Y.words, 1, questions, work_dir + 'analogy_results_1.txt')

    #Y = np.matrix([
    #    [1, 3],
    #    [3, 3],
    #    [0, -4],
    #    [-2, 4],
    #    [-1, -1],
    #])
    #questions = ['b c d e']
    #analogy_tests.answer_analogy_questions([Y], ['a', 'b', 'c', 'd', 'e'], 1, questions, work_dir + 'analogy_results_test.txt')

    settings.logger.info('Done...')