import codecs
import gensim
import numpy as np
import random
import scipy.spatial.distance

import settings

def cosine_similarity(a, b):
    return 1 - scipy.spatial.distance.cosine(a, b)

def cosine_similarities(A, b):
    #settings.logger.info('cosine_similarities.0')
    Ab = A.dot(b.T)
    #settings.logger.info('cosine_similarities.1')
    b_length = np.sqrt(np.multiply(b, b).sum(axis = 1))
    #settings.logger.info('cosine_similarities.2')
    A_lengths = np.sqrt(np.multiply(A, A).sum(axis = 1))
    #settings.logger.info('cosine_similarities.3')
    return np.array(Ab / b_length / A_lengths)

def cosine_similarities_normalized(A, b):
    return A.dot(b.T)

def get_word_vec(word, matrix):
    row = words.index(word)
    return matrix[row]

def get_word_vec_(words, word, matrix):
    row = words.index(word)
    return matrix[row]

def word_similarity(word1, word2, matrix):
    vec1 = get_word_vec(word1, matrix)
    vec2 = get_word_vec(word2, matrix)
    return cosine_similarity(vec1, vec2)

def word_similarity_(words, word1, word2, matrix):
    vec1 = get_word_vec_(words, word1, matrix)
    vec2 = get_word_vec_(words, word2, matrix)
    return cosine_similarity(vec1, vec2)

def rank_synonym(word, synonym, matrix):
    similarity = word_similarity(word, synonym, matrix)
    similarities = cosine_similarities(matrix, get_word_vec(word, matrix))
    return sum(round(similarity, 6) < np.round(similarities, 6))

def rank_synonym_(words, word, synonym, matrix):
    similarity = word_similarity_(words, word, synonym, matrix)
    similarities = cosine_similarities(matrix, get_word_vec_(words, word, matrix))
    return sum(round(similarity, 6) < np.round(similarities, 6))

def rank_synonyms(Y, Wi, words, results_file, synonyms = None, limit = 1000):
    if synonyms is None:
        synonyms = settings.get_synonyms()
    #random.shuffle(synonyms)
    count = 0
    with codecs.open(results_file, 'w', 'utf-8', buffering = 0) as file:
        for pair in synonyms:
            if not ((pair[0] in words) and (pair[1] in words)):
                continue
            settings.logger.info("Rank \"" + pair[0] + "\" and \"" + pair[1] + "\"")
            file.write(pair[0] + '\t' + pair[1] + '\t')
            rank = rank_synonym_(words, pair[0], pair[1], Y)
            file.write("%d" % rank)
            for W in Wi:
                rank = rank_synonym_(words, pair[0], pair[1], W)
                file.write("\t%d" % rank)
            file.write("\n")
            count += 1
            if count == limit:
                break

if __name__ == "__main__":
    settings.logger.info("Loading words")
    words = settings.get_words()
    #model = gensim.models.Word2Vec.load_word2vec_format(settings.get_w2v_model_filename(0), binary = True)
    #models = [gensim.models.Word2Vec.load_word2vec_format(settings.get_w2v_model_filename(i), binary = True) for i in range(settings.model_count)]

    settings.logger.info("Loading Y")
    Y = np.load(settings.Y_filename)

    settings.logger.info("Loading Wi")
    Wi = [np.load(settings.get_w2v_matrix_filename(i)) for i in range(settings.model_count)]

    #synonyms = [("maja", "hoone"), ("organism", "elusolend"), ("ilus", "kena"), ("halb", "paha")]
    with open(settings.word2vec_models_path + "synonyms.txt", "r") as f:
        synonyms = [line.rstrip("\n").split(" ") for line in f]

    words_set = set(words)
    synonyms = [pair for pair in synonyms if pair[1] in words_set]

    with open(settings.results_path + "synonym_ranks_test.txt", "w") as file:
        for pair in synonyms:
            settings.logger.info("Rank \"" + pair[0] + "\" and \"" + pair[1] + "\"")
            rank = rank_synonym(pair[0], pair[1], Y)
            if pair[0] == 'vilets':
                rank = 1
            file.write("%d" % rank)
            for W in Wi:
                rank = rank_synonym(pair[0], pair[1], W)
                file.write("\t%d" % rank)
            file.write("\n")

    #with open(settings.results_path + "synonym_similarities.txt", "w") as file:
    #    for pair in synonyms:
    #        settings.logger.info("Similarities of \"" + pair[0] + "\" and \"" + pair[1] + "\"")
    #        similarity = word_similarity(pair[0], pair[1], Y)
    #        file.write("%.8f" % similarity)
    #        for W in Wi:
    #            similarity = word_similarity(pair[0], pair[1], W)
    #            file.write("\t%.8f" % similarity)
    #        file.write("\n")

    np.random.seed(0)
    settings.logger.info("Comparing similarities of random pairs of words")
    with open(settings.results_path + "random_similarities.txt", "w") as file:
        for i in range(1000):
            i1, i2 = np.random.choice(len(words), 2)
            word1 = words[i1]
            word2 = words[i2]
            similarity = word_similarity(word1, word2, Y)
            file.write("%d\t%d\t%.8f" % (i1, i2, similarity))
            for W in Wi:
                similarity = word_similarity(word1, word2, W)
                file.write("\t%.8f" % similarity)
            file.write("\n")
            if i % 100 == 99:
                settings.logger.info("%d pairs of words compared" % (i + 1))
