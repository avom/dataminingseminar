import codecs
import numpy as np
import settings

def export_matrix_to_text(filename, out_filename, words):
    mat = np.load(filename)
    with codecs.open(settings.results_path + out_filename, "w", "utf-8") as outf:
        for i in range(len(words)):
            outf.write(words[i] + "\t" + str(list(mat[i]))[1:-1].replace(',', '') + "\n")

if __name__ == "__main__":

    settings.logger.info("Loading words...")
    words = settings.get_words()

    settings.logger.info("Exporting Y...")
    export_matrix_to_text(settings.Y_filename, "Y.txt", words)

    settings.logger.info("Exporting W0...")
    export_matrix_to_text(settings.get_w2v_matrix_filename(0), "W0.txt", words)

