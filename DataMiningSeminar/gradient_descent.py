from numpy import linalg

import numpy as np
import random

import iterative_minimization
import settings

def generateMatrix(m, n):
    return np.matrix([[round(random.random() * 2 - 1, 1) for i in range(n)] for j in range(m)])

def score(Y, W, P):
    diff = Y - np.matmul(W, P)
    return np.sum(np.multiply(diff, diff))

def sdfP(Y, W, P, row, col):
    diff = Y[:, col] - np.matmul(W, P[:, col])
    return -2 * (np.matmul(diff.transpose(), W[:, row])[0, 0])

def gradient_descent(W, u, numSteps):
    m, n = W.shape
    Y = generateMatrix(m, n)
    P = generateMatrix(n, n)
    for step in range(0, numSteps):
        row = random.randint(0, n - 1)
        col = random.randint(0, n - 1)
        P[row, col] -= u * sdf(Y, W, P, row, col)
        if (step + 1) % 10000 == 0:
            error = iterative_minimization.calcMeanRMSE([W.dot(P)], Y)
            settings.logger.info("Error: %.14f" % error)
    return (Y, P)

random.seed(0)
m = 3 # objects/words
n = 2 # features/dimensions
r = 4 # experiments/models
settings.logger.info("Generating W1...")
#W1 = generateMatrix(m, n)
W1 = np.matrix([[0.1, 0.2], [0.3, 0.4], [0.5, 0.6]])


Y, P = gradient_descent(W1, 0.1, 10 ** 6)

print(Y)
print(P)

iterative_minimization.calculateY([W1], use_scaling = True)
