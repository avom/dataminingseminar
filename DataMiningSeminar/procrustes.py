import numpy as np
import scipy.linalg

def procrustes(A, B):
    S = A.T.dot(B)
    StS = S.T.dot(S)
    SSt = S.dot(S.T)
    V, Ds1, Vt = np.linalg.svd(StS)
    W, Ds2, Wt = np.linalg.svd(SSt)
    T = W.dot(Vt)
    return T
    
def calculateP(Y, W):
    P, scale = scipy.linalg.orthogonal_procrustes(W, Y)
    return P
#np.random.seed(0)
#Y = np.round(np.random.rand(3, 2) * 10 - 5)
#W = np.matrix([[1, 2], [3, 4], [5, 6]])

#print(Y)
#print(W)
#print(procrustes(W, Y))

#V, D, Vt = procrustes(W, Y)

#R, scale = scipy.linalg.orthogonal_procrustes(W, Y)

#print(R)
#print(scale)

#P = procrustes(W, Y)
#print(W.dot(P))