\documentclass[handout]{beamer}
%\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{mleftright} 
\usepackage{ragged2e}


\title{Quantifying the variability of the word embedding spaces}
\subtitle{Data mining seminar project}
\author{Avo Muromägi}
\date{University of Tartu\\2016}

\addtobeamertemplate{block begin}{}{\justifying}

%\AtBeginSection[]
%{
%	\begin{frame}
%		\frametitle{Table of Contents}
%		\tableofcontents[currentsection]
%	\end{frame}
%}
\DeclareMathOperator*{\Prb}{\mathbf{P}}
\DeclareMathOperator*{\Exp}{\mathbf{E}}
\DeclareMathOperator*{\I}{\mathbf{Inf}}
\DeclareMathOperator{\bias}{bias}
\DeclareMathOperator{\nstab}{Stab}
\DeclareMathOperator{\parity}{PAR}

\providecommand{\poly}{\operatorname*{poly}}

\newcommand{\abs}[1]{\left\lvert #1 \right\rvert}

\newcommand{\NN}{\mathbb{N}}

\newcommand{\bigO}[1]{{O\mleft( #1 \mright)}}
\newcommand{\bigOmega}[1]{{\Omega\mleft( #1 \mright)}}

\newcommand{\eqdef}{\stackrel{\rm def}{=}}
\newcommand{\setOfSuchThat}[2]{ \left\{\; #1 \;\colon\; #2\; \right\} } 			
\newcommand{\indicSet}[1]{\mathds{1}_{#1}}                                              
\newcommand{\indic}[1]{\indicSet{\left\{#1\right\}}}                                             
\newcommand{\disjunion}{\amalg}

\newcommand{\proba}{\Prb}
\newcommand{\probaOf}[1]{\proba\!\left[\, #1\, \right]}

\newcommand{\expbias}[2]{\operatorname{ExpBias}_{#1}(#2)}

\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\newcommand{\matvalue}[1]{\mathbf{#1}}

\newcommand{\mypage}[1]{
\begin{frame}
	\begin{centering}
		\begin{beamercolorbox}[sep=12pt,center]{part title}
			\usebeamerfont{section title}#1\par
		\end{beamercolorbox}
	\end{centering}
\end{frame}
}

\setbeamertemplate{bibliography item}[text]

\begin{document}
	\begin{frame}
		\titlepage
	\end{frame}

	\mypage{Introduction}

	\begin{frame}
		\frametitle{What is word embedding?}
		A set of techniques in NLP where words or phrases are mapped to vectors of real numbers.
	\end{frame}

	\begin{frame}
		\frametitle{Word2Vec}
		\begin{itemize}
			\item<1-> A group of models producing word embeddings from large corpus of text.
			\item<2-> Maps a vector to each word in vocabulary.
			\item<3-> Not deterministic. Produced embeddings are different after each run.
			\item<4-> Simpler, but faster approach compared to alternatives.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Word2Vec model}
		\begin{itemize}
			\item<1-> Contains $n$ dimensional vectors $w_1, \ldots, w_m$ representing $m$ words in the vocabulary.
			\item<2-> Assemble the word in to matrix a \[W =
			\begin{pmatrix}
				w_{11}, \ldots, w_{1n}\\
				\ldots, \ldots, \ldots\\
				w_{m1}, \ldots, w_{mn}
			\end{pmatrix}\]
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Multiple Word2Vec models}
		\begin{itemize}
			\item<1-> Each Word2Vec run produces a different model (matrix).
			\item<2-> Run Word2Vec $r$ times.
			\item<3-> We now have matrices $W_1, \ldots, W_r.$
			\item<4-> Can we combine them into better model $Y?$
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{A better model?}
		\begin{itemize}
			\item<1-> Find translation matrices $P_1, \ldots, P_r$ and a target matrix $Y$ so that 
				\[\sum_{i=1}^r \norm{Y - W_i P_i}^2 \rightarrow \text{min.}\]
			\item<2-> Avoid obvious solution $P_i = \matvalue 0$, $Y = \matvalue 0.$
			\item<3-> Is $Y$ better?
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{What we used?}
		\begin{itemize}
			\item<1-> Models trained on Estonian Reference Corpus were provided to us.
			\item<2-> Vectors had $50, 100, \ldots, 300$ dimensions.
			\item<3-> $10$ models for each dimension trained on Estonian language.
			\item<4-> About $800,000$ words in a vocabulary.
		\end{itemize}
	\end{frame}

	\mypage{Methods}

	\begin{frame}
		\frametitle{Linear regression coefficient estimates}
		Given $W_1, \ldots, W_r$ and random $Y$ do the following iteratively until error starts converging.
		\begin{enumerate}
			\item<1-> Scale $Y$ so that variance of each column is 1.
			\item<2-> Solve $Y - W_i P_i = \matvalue 0$ for each $P_i$.
				\[P_i = (W_i^T W_i)^{-1} W_i^T Y\]
			\item<3-> Compute $Y = \frac 1 r \sum_{i=1}^r W_i P_i.$
			\item<4-> Calculate error $\sum_{i=1}^r \norm{Y - W_i P_i}^2$.
		\end{enumerate}
	\end{frame}

	\begin{frame}
		\frametitle{Orthogonal Procrustes problem}
		Given matrices $W$ and $Y$, find $P$ so that:
		\begin{align*}
			W P &= Y + E\\
			P P^T &= P^T P = I\\
			tr(E^T E) &= min,
		\end{align*}
		where $E$ is an error matrix and $tr(E^T E)$ is a sum of $E^T E$ elements.
	\end{frame}

	\begin{frame}
		\frametitle{Solving Orthogonal Procrustes problem}
		\begin{itemize}
			\item<1-> Compute $S = W^T Y$.
			\item<2-> Then using SVD to diagonalize
				\begin{align*}
					S^T S &= V D_S V^T\\
					S S^T &= U D_S U^T
				\end{align*}
			\item<3-> Finally calculate translation matrix $P = UV^T.$
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Using Orthogonal Procrustes problem}
		\begin{itemize}
			\item<1-> Use iterative approach like with linear regression solution.
			\item<2-> Has the advantage of translation matrices $P_i$ being orthogonal:
			\begin{itemize}
				\item<1-> Vector lengths and angles between vectors are preserved in $W_i P_i.$
				\item<2-> No need to worry about the trivial solution.
			\end{itemize}
		\end{itemize}
	\end{frame}

	\mypage{Measurements}

	\begin{frame}
		\frametitle{Word distance}
		\begin{itemize}
			\item<1-> Find distance of a word from each $W_i P_i$ to $Y$?
			\item<2-> Square all calculated distances.
			\item<3-> Calculate the mean of squared distances for each word.
		\end{itemize}
		
		%\includegraphics[width=0.8\linewidth]{images/word_dist_vis}
	\end{frame}

	\begin{frame}
		\frametitle{Word distances}
		\centering
		\includegraphics[width=0.9\linewidth]{images/word_distances_sample_50}
	\end{frame}

	\begin{frame}
		\frametitle{Similarities}
		\begin{itemize}
			\item<1-> Choose a number of pairs of words randomly.
			\item<2-> Calculate their (cosine) similarity in $Y, W_1, \ldots, W_r.$
			\item<3-> Order the pairs according to their similarity in $Y.$
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Similarities}
		\centering
		\includegraphics[width=0.9\linewidth]{images/random_similarities}
	\end{frame}

	\begin{frame}
		\frametitle{Synonym test}
		\begin{itemize}
			\item<1-> We chose a pair of synonyms,
			\item<2-> For one of the words computed all similarities in $Y, W_1, \ldots, W_r.$
			\item<3-> Then found ranks of similarites of the other word.
			\item<4-> Total of 1000 synonym pairs.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Synonym test}
		\centering
		\begin{tabular}{rrrr}
		\hline
		  & Linear reg. & Procrustes & \\
		Dims & Mean $Y$ & Mean $Y$ & Mean $W$ \\
		\hline
		 50 & 70098 & 38998 & 41933 \\
		100 & 68175 & 32485 & 35986 \\
		150 & 73182 & 30249 & 33564 \\
		200 & 73946 & 29310 & 32865 \\
		250 & 75884 & 28469 & 32194 \\
		300 & 77098 & 28906 & 32729 \\
		\hline
		\end{tabular}
	\end{frame}

	\begin{frame}
		\frametitle{Analogy test}
		\begin{itemize}
			\item<1-> king - man + woman = ?
			\item<2-> Use the most similar word to the above as the prediction.
			\item<3-> With 259 quartets of Estonian words.
			\item<4-> Find the fraction of correctly made predictions.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Analogy test results}
		\centering
		\begin{tabular}{rrrrr}
		\hline
		Dimensions & Linear & Procrustes & Inital \\
		\hline
		 50 & 0.023 & 0.062 & 0.027 \\
		100 & 0.027 & 0.039 & 0.032 \\
		150 & 0.008 & 0.027 & 0.041 \\
		200 & 0.027 & 0.046 & 0.051 \\
		250 & 0.015 & 0.062 & 0.052 \\
		300 & 0.008 & 0.039 & 0.055 \\
		\hline
		\end{tabular}
	\end{frame}

	\begin{frame}
		\frametitle{Analogy test results - correct word in top 10}
		\centering
		\begin{tabular}{rrrrr}
		\hline
		Dimensions & Linear & Procrustes & Inital \\
		\hline
		 50 & 0.073 & 0.162 & 0.131 \\
		100 & 0.073 & 0.166 & 0.153 \\
		150 & 0.093 & 0.189 & 0.173 \\
		200 & 0.085 & 0.205 & 0.198 \\
		250 & 0.073 & 0.224 & 0.197 \\
		300 & 0.108 & 0.216 & 0.206 \\
		\hline
		\end{tabular}
	\end{frame}

	\begin{frame}
		\frametitle{Conclusion}
		\begin{itemize}
			\item<1-> Similar words are more similar in $Y$ from both methods.
			\item<2-> Distant words are even more distant in $Y$ from both methods.
			\item<3-> Orthogonal Procrustes problem approach looks better.
			\item<4-> $Y$ attained via Procrustes problem solution may be better than initial models.
			\item<5-> $Y$ attained via linear regression approach is worse than initial models.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{The end!}
		\centering
		Questions?
	\end{frame}

	% \mypage{What are we trying to learn?}
	% \begin{frame}
	% 	\frametitle{Boolean functions}
	% 	\begin{itemize}
	% 	\item<1-> Boolean function $f:\{0,1\}^n\rightarrow\{0,1\}$
	% 	\item<2-> Class of Boolean functions $C\subseteq\{0,1\}^{\{0,1\}^n}$
	% 	\vspace{0.6cm}
	% 	\item<3-> For a known class $C$ we want to learn unknown $f\in C$.
	% 	\end{itemize}
	% 	\include{./tikz/boolean_cube3}
	% \end{frame}
	
\end{document}
